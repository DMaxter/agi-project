# LAB 03 Report - ist189429

AGISIT 20201-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png)


| Number | Name              | Username                                     | Email                               |
| -------|-------------------|----------------------------------------------| ------------------------------------|
| 89429  | Daniel Matos      | https://git.rnl.tecnico.ulisboa.pt/ist189429 | mailto:daniel.m.matos@tecnico.ulisboa.pt   |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command terraform init which plugins were installed? (You can copy the result of the command to insert in your report).

When I ran the command `terraform init`, it installed the plugin `google` so terraform could interact with Google Cloud Platform and manage the infrastructure there.

The output of the command is:

```
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/google...
- Installing hashicorp/google v3.90.0...
- Installed hashicorp/google v3.90.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

### Q02: Analyze briefly the `terraform-gcp-servers.tf` and interpret its purpose.

The file `terraform-gcp-servers.tf` is where I defined the machines I want to use.
In this case, we define the keypair to be used, what type of instance is the balancer and with what image, as well as the network where it belongs. It also says the same thing for the web intances, along with the number of web servers to launch.

### Q03: Analyze briefly the `terraform-gcp-variables.tf` and interpret its purpose.

The file `terraform-gcp-variables` is where I defined some variables that will be used in the other files, so I don't need to repeat them if I wanted to change machine types, or region for example.

### Q04: If you would need more Web servers where and how would you declare that intention (with Terraform)?

If I would need more servers, I would just go to the file `terraform-gcp-servers.tf` and edit the number to the desired amount, by changing the value of `count`.

### Q05: Which other files appeared in the gcpcloud folder after running `terraform apply` for your infrastructure?

After running `terraform apply`, a new file named `terraform.tfstate` where the state about the deployed infrastructure and configuration is stored.

### Q06: After creating the infrastructure with Terraform, you needed to modify the gcphosts inventory. What was changed in that file, and what was the purpose?

I needed to set the IPs of the `balancer` and of the multiple `web` instances in the `myhosts` file, so that ansible could know where the machines were located.


## Deploy and Configure the Web Service with Ansible

### Q07: After creating the infrastructure with Terraform, you have tested the communication with the remote instances using an Ansible ad-hoc command. **Copy the result into your report. Was the result successful? In case of positive answer, can you explain why it worked, i.e., how was Ansible capable of establishing a session with the remote instances?**

To test the communication with the remote instances I used the command `ansible targets -m ping` and it was successful.
This happens because I have set the location of our public key in the `terraform.tfvars` file, so when the instances were deployed, our key was automatically added to the `authorized_keys` file of each instance, so the command ran successfully.

The output was:

```
$ ansible targets -m ping
web2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
balancer | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
web1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
web3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

### Q08: Which files did you had to modify, so that Terraform and Ansible could Provision and Configure correctly the instances? Explain in detail what were the modifications.

To correctly configure the instances, I had to make changes in 2 different files:

- Lines 59 and 116 in `ansible-gcp-servers-setup-all.yml`: the path `/home/vagrant/gcpcloud-tenant/templates` is unexistent so it should be `/home/vagrant/labs/gcpcloud/templates`

- Line 5 in `ansible.cfg`: the path `/home/vagrant/gcpcloud-tenant/gcphosts` is unexistent so it should be `/home/vagrant/labs/gcpcloud/gcphosts`

### Q09: When the system was fully deployed, when hitting the refresh button on the web browser with the address of the Load Balancer, **What changed?** Describe those changes and interpret why they happened (if indeed happened). Which IP addresses of the instances were Shown, and what do they correspond to?

When the web page was refreshed, a different IP and hostname were shown each time, in a round-robin way, going from `web1` to `web3`. This happens due to the load balancing provided by the HA Proxy running in the `balancer` instance.

## Destroying the Infrastructure

### Q10: When you run terraform destroy what was the result after you have confirmed the question "Do you really want to destroy all resources?". Copy those results to your report. After the destroy, looking at the Google Cloud Platform Dashboard, namely in the ACTIVITY tab, what information is displayed so that you are sure the resources have really been destroyed?

When I ran `terraform destroy`, it destroyed the resources it had allocated previously.
When looking at the Google Cloud Platform Dashboard, in the `Activity` tab, all the allocated resources have been destroyed.

The final output of this command was:

```
google_compute_instance.web[2]: Destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web3]
google_compute_instance.web[1]: Destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web2]
google_compute_instance.web[0]: Destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web1]
google_compute_instance.balancer: Destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/balancer]
google_compute_firewall.frontend_rules: Destroying... [id=projects/agi-89429/global/firewalls/frontend]
google_compute_instance.web[2]: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web3, 10s elapsed]
google_compute_instance.web[1]: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web2, 10s elapsed]
google_compute_instance.web[0]: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web1, 10s elapsed]
google_compute_firewall.frontend_rules: Still destroying... [id=projects/agi-89429/global/firewalls/frontend, 10s elapsed]
google_compute_instance.balancer: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/balancer, 10s elapsed]
google_compute_firewall.frontend_rules: Destruction complete after 11s
google_compute_instance.web[2]: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web3, 20s elapsed]
google_compute_instance.web[1]: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web2, 20s elapsed]
google_compute_instance.web[0]: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/web1, 20s elapsed]
google_compute_instance.balancer: Still destroying... [id=projects/agi-89429/zones/europe-west3-a/instances/balancer, 20s elapsed]
google_compute_instance.web[2]: Destruction complete after 21s
google_compute_instance.web[1]: Destruction complete after 21s
google_compute_instance.balancer: Destruction complete after 21s
google_compute_instance.web[0]: Destruction complete after 22s
```
