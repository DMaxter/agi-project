# Lab-03 README

AGISIT 20201-2022

## Authors

**Team 08A**

![Tiago](../photos/ist189542-photo.png)


| Number | Name              | Username                                       | Email                                        |
| -------|-------------------|------------------------------------------------| ---------------------------------------------|
| 89542  | Tiago Fonseca     | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt>   |


# Provisioning the Infrastructure with Terraform

## Q01: When you run the command terraform init which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed one plugin called `hashicorp/google`, that is the Google provider used to configure Google Cloud Platform infrastructure.

![terraform init](pictures/89542_q1.png)

---

## Q02: Analyze briefly the terraform-gcp-servers.tf and interpret its purpose.

The main purpose of this file is to configure the amount of desire `Web Servers` (line 10), as well as the `load balancer` and all the necessary configurations. Both share a lot of configurations, for instance the `name` of the server, followed by the `machine_type` and `zone` (lines 11, 12 and 13). There are some other configurations, like the `boot_disk` configuration as well as the `network interface`. Finally it also allows us to configure the information about the `ssh-keys` assigned to each server (store on the .ssh/id_rsa.pub). All this information allows us to controll the way that the servers are created and deployed. 

---

## Q03: Analyze briefly the terraform-gcp-variables.tf and interpret its purpose.

The main purpose of this file is to define some crucial variables for the terraform-gcp-servers.tf. For instance the `GCP_PROJECT_NAME`, used to identify the project that is being run on the GCP. The `GCP_MACHINE_TYPE` that defines the machine type selected, that can have better or worst specs, depending on the price. Finally the `GCP_REGION` that determines the region where the deployment will be performed. There is also the `DISK_SIZE` that establishes the amount of space desired on the disk.

---

## Q04: If you would need more Web servers where and how would you declare that intention (with Terraform)?

I would change the count value on the line 10 of `terraform-vmcloud-servers.tf`, from 3 to 4. 
To have more than 3 servers, we would have to change this value to a bigger number, and we would have to add new ips to the `gcphosts` file (as well as the name of those servers below web and targets regions).

---

## Q05: Which other files appeared in the gcpcloud folder after running terraform apply for your infrastructure? .

Before running terraform apply, there fils present were the ones that were already on the repository, for instance all the terraform-gcp files as well as all the ansible configuration files. 

After running the `terraform apply` two files appeared:

- The file `terraform.tfstate`. After some analysis on the file, I understood that it is a file were all the terraform state is stored. After some more investigation on the subject, I learned that it is where terraform stores all the state about our managed infrastructure and configuration. This is also used whenever we want to create a plan and make changes on our infrastructure. 
The main is than to store a binding between objects in a remote system and resource instances that are declared in our configuration.

- The file `terraform.tfstate.backup`. This file is pretty much the same as the previous one, except that as the name indicates, this one is a backup. It is used in the case that the original state file is lost or corrupted, allowing us to recover our configuration.

---

## Q06: After creating the infrastructure with Terraform, you needed to modify the gcphosts inventory. What was changed in that file, and what was the purpose?

After running terraform apply, the infrastructure was created and deployed in the `GCP`. In order for Ansible to access the machines and configure them, I had to populate the `gcphosts` file, that in this case is the inventory. I changed the IP values of all the servers and the load balancer (since they all were hardcoded as XXXXXXX) to the values given by the output of terraform.

This step is important for the `ansible` to be able to access and configure the remote machines. Having access to those IP's is a crucial part for it to work.  

---

# Deploy and Configure the Web Service with Ansible

## Q07: After creating the infrastructure with Terraform, you have tested the communication with the remote instances using an Ansible ad-hoc command. Copy the result into your report. Was the result successful? In case of positive answer, can you explain why it worked, i.e., how was Ansible capable of establishing a session with the remote instances?

The result is here

![communications](pictures/89542_q7.png)

For the simple ping using the ICMP protocol, I had to access the system `hosts`  file and added all the different IP's I got from the terraform apply. It allowed the system host to know all the new IP's and allows the ping to work.

For the Ansible ad-hoc "Ping-Pong", the answer to the previous question is one of the reasons why it worked. Since ansible had access to all the IPs of the machines, ansible was able to contact all the servers. Other reason is the fact that the web servers created include the public ssh key that is present on the ``mgmt`` node. It allows us to securely connect via `ssh` withouth having to use a password. This keypair togheter with the population of the `inventory` file with the IP's from all the servers and load balancer, allowed ansible to contact them due to the IP provided from the terraform apply output.

---

## Q08: Which files did you had to modify, so that Terraform and Ansible could Provision and Configure correctly the instances? Explain in detail what were the modifications.

Firstly I had issues with the configured zone. I had to change the file ``terraform-gcp-variables`` in order to configure a new region. I selected ``europe-west1-c``.

Than I tried to run the ansible ad-hoc "Ping-Pong" (previous question). Unfortunately some erros ocurred. It was a simple one, there was a typo on the ``ansible.cfg``. The path to the inventory was wrong, so I had to change it to ``/home/vagrant/labs/gcpcloud/gcphosts``..

This solved the ping, and allowed me to complete the previous question. Than when I tried to run the playbook file, another error ocurred. Luckily the error was the same as before, and all I had to do was fixing the values that had a faulty path. In this case the problem was on the ``ansible-gcp-servers-setup-all.yml``. This file had two faulty paths on line 59 and line 116. I corrected it to ``/home/vagrant/labs/gcpcloud/templates/index.html.j2`` and ``/home/vagrant/labs/gcpcloud/templates/haproxy.cfg.j2`` respectively.

After that, everything worked as expected.

---

## Q09: When the system was fully deployed, when hitting the refresh button on the web browser with the address of the Load Balancer, What changed? Describe those changes and interpret why they happened (if indeed happened). Which IP addresses of the instances were Shown, and what do they correspond to?

This is an example of what happened when I pressed the refresh button:

Before refresh                             |  After refresh
:-----------------------------------------:|:------------------------------------------:
![before-refresh](pictures/89542_q9_1.png) | ![after-refresh](pictures/89542_q9_2.png)

Everytime we hit the refresh button, the browser changed to a different server. This is because of the Load Balancer, that is distributing the traffic across all the available servers. It is also the same behavior of the previous lab, which means that everyting worked, but this time using Terraform with Ansible.

The ip adresses shown correspond to the internal ips of each webserver instead of the public one.

---

# Destroying the Infrastructure

## Q10: When you run terraform destroy what was the result after you have confirmed the question "Do you really want to destroy all resources?". Copy those results to your report. After the destroy, looking at the Google Cloud Platform Dashboard, namely in the ACTIVITY tab, what information is displayed so that you are sure the resources have really been destroyed?

The Google Cloud Platform had the following result

Before destroy                             |  After destroy
:-----------------------------------------:|:------------------------------------------:
![before-destroy](pictures/89542_q10_1.png) | ![after-destroy](pictures/89542_q10_2.png)

Looking at the `Google Cloud Platform`, there are no vm instances which means that all the resources were completly destroyed as expected.

![activity-tab](pictures/89542_q10_3.png)

The activity tab also confirmed the deletion of all the resources, meaning that ``terraform destroy`` worked as expected and destroyed all the resources.



