# Lab 03 Report - ist189475

AGISIT 2021-2022

## Authors

**Team 08A - João Soares**

![João Soares](../photos/jsoares.jpg)

| Number | Name        | Username                                       | Email                                     |
| ------ | ----------- | ---------------------------------------------- | ----------------------------------------- |
| 89475  | João Soares | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt> |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command `terraform init` which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed 1 plugin: hashicorp's `google` module. The output of the command can be seen below:

![Output of `terraform init`](pictures/89475_q1.png)

### Q02: Analyze briefly the `terraform-gcp-servers.tf` and interpret its purpose.

This file contains the configurations for the servers to be deployed. Since terraform is declarative, the contents only state the desired configuration for the whole system, in this case regarding the servers.

There are 2 main resources being created: the web servers and the load balancer. Both of them are of type _google_compute_instance_ and share a lot of the same configuration parameters, namely the boot_disk initialization params, which specify the image of the node (in this case `ubuntu-2004-lts`); the machine's type and zone, which are set in the `terraform-gcp-variables.tf` file; the network interface, and the public ssh key of the mgmt local vagrant node, in the metadata options. The only fields that differ between both resources are the name and the tags: the balancer has a name and tag of "balancer", and the web servers, being 4, have the name of "web1", "web2", "web3" and "web4", and all have the same tag "web".

### Q03: Analyze briefly the `terraform-gcp-variables.tf` and interpret its purpose.

This file contains the variables that the other files can use to create the infrastructure. The variables are the project name (in gcp cloud), the machine type (n1-standard-1 in this case), the region for the nodes (europe- west3-c, which had to be changed to europe-west3-a), the size of the disks (15gb) and the image the machines use (`ubuntu-2004-lts`). These values can then be used in another tf file by calling `var.NAME_OF_VAR`.

### Q04: If you would need more Web servers where and how would you declare that intention (with Terraform)?

The `terraform-gcpcloud-servers.tf` file was populated with 3 web servers by default. In case we would require more web servers we would simply increase this value to our desired number.

We would also have to update the `gcphosts` inventory file with more web server entries and their corresponding ips.

In our case, we increased the web server count from **3** to **4**.

### Q05: Which other files appeared in the `gcpcloud` folder after running `terraform apply` for your infrastructure?

Upon running the specified command, a new file was created: `terraform.tfstate`. This file contains all the information about the current state of the deployed infrastructure as well as all the defined outputs in the `terraform-gcp-outputs.tf` configuration file. In case it is not the first time that the command is run, a backup file `terraform.tfstate.backup` is created as well.

### Q06: After creating the infrastructure with Terraform, you needed to modify the `gcphosts` inventory. What was changed in that file, and what was the purpose?

Since the web servers were just created, there was no way of knowing which ip each node would have. This is a crucial piece of information for ansible to work properly. For that reason, we populated this file with the correct ips outputed by terraform, so that ansible can know which machines to connect to.

## Deploy and Configure the Web Service with Ansible

### Q07: After creating the infrastructure with Terraform, you have tested the communication with the remote instances using an Ansible ad-hoc command. Copy the result into your report. Was the result successful? In case of positive answer, can you explain why it worked, i.e., how was Ansible capable of establishing a session with the remote instances?

The ping command was successful. This is due to the fact that ansible not only knew the ips of the servers to connect to, which were edited in the `gcphosts` file, but also because the web servers were created with metadata that included the mgmt's (local vagrant node) public ssh key. This key allows us to connect securely via `ssh` without prompting for a password. Since ansible is **agentless** and connects via `ssh`, this was all that was required to achieve a successful connection with the remote instances.

![Output of `ansible ping`](pictures/89475_q7.png)

### Q08: Which files did you have to modify, so that Terraform and Ansible could Provision and Configure correctly the instances? Explain in detail what were the modifications.

Initially, the `gcphosts` file had to be updated with the correct number of instances as well as the corresponding ip addresses. The `terraform-gcpcloud-servers.tf` file was also updated with 1 more web server. However these changes were performed and described earlier.

When we tried running `terraform apply`, the GCP module showed a few errors in the console:
![Output of error of `terraform apply`](pictures/89475_q8.png)

Apparently the zone we were trying to use was not available so we needed to change to a different one, i.e. zone a. To fix these errors, we simply changed the zone from `europe-west3-c` to `europe-west3-a` in the file `terraform-gcp-variables.tf`.

After fixing that error, we tried running an ansible ad-hoc ping command. However this command did not execute successfully at first. Upon further inspection, we had to fix the path for ansible's inventory file (from `/home/vagrant/gcpcloud-tenant/gcphosts` to `/home/vagrant/labs/gcpcloud/gcphosts`) in `ansible.cfg`.

Running ansible's playbook didn't work at first either. The bug was the same as the one above, where the path to the template files was outdated. Simply changing the paths to the correct one (`/home/vagrant/labs/gcpcloud/gcphosts`) in the `ansible-gcp-servers-setup-all.yml` file fixed this issue.

### Q09: When the system was fully deployed, when hitting the refresh button on the web browser with the address of the Load Balancer, What changed? Describe those changes and interpret why they happened (if indeed happened). Which IP addresses of the instances were Shown, and what do they correspond to?

Since we're acessing the web servers indirectly via the load balancer, our request can end up in any of the 4 deployed web servers. When ansible copied the `index.html` file that is served by the web node when we access the website, it was populated with the requested variables, namely the hostname and the ip address. This means that whenever we hit refresh, we can visibly see which web node we are being forwarded to. Furthermore, the ip address displayed on the web page corresponds to the private address within google's network, and not the public one. For example, lets say web3 is the one serving our request and its public ip is `35.246.226.32`. On the webpage it would display `web3 (10.156.0.4)` instead, which is its private ip address.

## Destroying the Infrastructure

### Q10: When you run `terraform destroy` what was the result after you have confirmed the question "Do you really want to destroy all resources?". Copy those results to your report. After the destroy, looking at the Google Cloud Platform Dashboard, namely in the ACTIVITY tab, what information is displayed so that you are sure the resources have been really destroyed?

Upon running `terraform destroy`, the output was the following:

![Output of `terraform destroy`](pictures/89475_q10_a.png)

Looking at the Google Cloud Platform Dashboard, there were no resources left from the entire deployment: terraform did in fact destroy all of the infrastructure it had created and provisioned beforehand.

The dashboard showed the amount of resources being used prior to their destruction:

![Resources before destruction](pictures/89475_q10_b.png)

And this was after:

![Resources before destruction](pictures/89475_q10_c.png)

The activity tab showed the deletion of all resources:

![Resources before destruction](pictures/89475_q10_d.png)
