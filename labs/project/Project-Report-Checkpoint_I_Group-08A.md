# Project Checkpoint I

AGISIT 20201-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png) ![João Soares](../photos/jsoares.jpg) ![Tiago](../photos/ist189542-photo.png)

| Number | Name          | Username                                       | Email                                      |
| ------ | ------------- | ---------------------------------------------- | ------------------------------------------ |
| 89429  | Daniel Matos  | <https://git.rnl.tecnico.ulisboa.pt/ist189429> | <mailto:daniel.m.matos@tecnico.ulisboa.pt> |
| 89475  | João Soares   | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt>  |
| 89542  | Tiago Fonseca | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt> |

## Module Leaders

| Module                                       | Leader        |
| -------------------------------------------- | ------------- |
| Architecture of the Solution                 | João Soares   |
| Configuration of the monitoring System       | ------------- |
| Compute nodes capabilities                   | ------------- |
| Networking and metric details                | Daniel Matos  |
| Provisioning of the Infrastructure           | Daniel Matos  |
| Scalability and High Availability Strategies | ------------- |
| Deployment of the Application                | Tiago Fonseca |

All other members of the group contributed in every single task, since we developed the project during the labs and in group sessions on discord.

## Pre-Requisites

To host our services, we chose to use the Google Cloud Platform (GCP). We are relying on Terraform, a tool to help us create and manage the infrastructure. After the creation of the nodes, we use Ansible to install the necessary dependencies, namely docker, and to copy and unzip each microservice zip file. We are using docker containers to run each service in its corresponding node.

The GCP project name is **AGISIT-2021-PROJECT-08A**.

## Project Files

```
project
├─ ansible
│   ├── ansible.cfg
│   ├── inventory (ansible inventory)
│   ├── inventory.template (template inventory used by helper script)
│   ├── populate-inventory.py (helper script used by hosts.sh)
│   └── servers-setup-all.yml (ansible playbook)
├── cleanup.sh (helper tool: clear ansible inventory)
├── hosts.sh (helper tool: copy ip of provisioned instances to ansible inventory)
├── id_rsa.pub (public key uploaded to the nodes)
├── pb (contains the proto files used in the source doe of the microservices)
├── src (contains the source code for all the microservices)
│   ├── adservice
│   ├── cartservice
│   ├── checkoutservice
│   ├── currencyservice
│   ├── emailservice
│   ├── frontend
│   ├── loadgenerator
│   ├── paymentservice
│   ├── productcatalogservice
│   ├── recommendationservice
│   ├── shippingservice
└── terraform
    ├── agisit-2021-project-08a-097c56533224.json (credentials for gcp)
    ├── genhosts.sh (helper script for hosts.sh)
    ├── myhosts (output of genhosts.sh, used by hosts.sh)
    ├── terraform-gcp-networks.tf
    ├── terraform-gcp-outputs.tf
    ├── terraform-gcp-provider.tf
    ├── terraform-gcp-servers.tf
    └── terraform-gcp-variables.tf
```

## Deployment

To deploy the services you need to initialize `Terraform` with `terraform init`, in order to satisfy some plugin requirements. You can then run `terraform plan` to view the changes that will be performed. Finally to provision the infrastructure, you must execute `terraform apply`.

Now that the infrastructure is created, in order for `Ansible` to access the machines and configure them, there is the need to create the `inventory` file that is in the ansible directory. To populate the file with the `IP addresses` we created a script called `hosts.sh` that is in the root directory. This tool fetches the IP addresses using the command `terraform output`, outputs them to a file named `myhosts` inside the terraform directory, and then creates a new file named `inventory` inside the ansible folder. This inventory file is derived from the template `inventory.template`. The script simply inserts each service's ip in its designated place.

With the inventory file populated, we must use `Ansible` to provision all the services. To achieve that, we only need to run the playbook `servers-setup-all.yml`.

### Current deployment

Currently, we have one VM per microservice and a bastion host, so we could access the VMs through SSH, since they don't have a public IP address.
We installed docker in each one of them and put the each service (zipped) in the correct VM. The docker images were built and the containers were started.
We were able to run all the containers except for the `cartservice` one because we are missing the redis container that it needs to access. Hopefully, this will be solved when we improve our deployment.

### Future Work

Later we plan to improve our deployment by using Kubernetes to orchestrate the containers instead of being us manually to do it.
We also need to make the metric system and deploy a monitoring system.
