# Project AGISIT 2021-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png) ![João Soares](../photos/jsoares.jpg) ![Tiago](../photos/ist189542-photo.png)

| Number | Name          | Username                                       | Email                                      |
| ------ | ------------- | ---------------------------------------------- | ------------------------------------------ |
| 89429  | Daniel Matos  | <https://git.rnl.tecnico.ulisboa.pt/ist189429> | <mailto:daniel.m.matos@tecnico.ulisboa.pt> |
| 89475  | João Soares   | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt>  |
| 89542  | Tiago Fonseca | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt> |

## Module Leaders

| Module                                       | Leader        |
| -------------------------------------------- | ------------- |
| Architecture of the Solution                 | João Soares   |
| Configuration of the monitoring System       | Tiago Fonseca |
| Compute nodes capabilities                   | João Soares   |
| Networking and metric details                | Daniel Matos  |
| Provisioning of the Infrastructure           | Daniel Matos  |
| Scalability and High Availability Strategies | Tiago Fonseca |
| Deployment of the Application                | Tiago Fonseca |

All the members of the group contributed in every single task, since we developed the project during the labs and in group sessions on discord.

## Prerequisites

To host our services, we chose to use the Google Cloud Platform (GCP), more specifically, Google Kubernetes Engine so it is easier to deploy our application. We are relying on Terraform, a tool to help us create and manage the infrastructure.  We are also using Terraform to manage the Kubernetes cluster, by launching our pods automatically.


The GCP project name is **AGISIT-2021-PROJECT-08A**.

## Architecture

We decided to deploy the Online Boutique application, and it has the following architecture:

![](https://i.imgur.com/KG8jl6S.png)

## Project Files

```
project
├── bootstrap-mgmt.sh
├── demo
│   └── README.md
├── pb
├── README.md
├── report
│   └── README.md
├── src
│   ├── adservice
│   ├── cartservice
│   ├── checkoutservice
│   ├── currencyservice
│   ├── emailservice
│   ├── frontend
│   ├── loadgenerator
│   ├── paymentservice
│   ├── productcatalogservice
│   ├── recommendationservice
│   └── shippingservice
├── terraform
│   ├── gcp-gke-main.tf
│   ├── gcp-gke-provider.tf
│   ├── gke
│   │   └── gcp-gke-cluster.tf
│   └── k8s
│       ├── k8s-ad.tf
│       ├── k8s-cart.tf
│       ├── k8s-checkout.tf
│       ├── k8s-currency.tf
│       ├── k8s-email.tf
│       ├── k8s-frontend.tf
│       ├── k8s-istio.tf
│       ├── k8s-monitoring.tf
│       ├── k8s-namespaces.tf
│       ├── k8s-payment.tf
│       ├── k8s-productcatalog.tf
│       ├── k8s-provider.tf
│       ├── k8s-recommendation.tf
│       ├── k8s-services.tf
│       ├── k8s-shipping.tf
│       ├── k8s-variables.tf
│       └── monitoring
│           ├── grafana.yaml
│           └── prometheus.yaml
└── Vagrantfile
```

## Deployment

To deploy the services you need to initialize `Terraform` with `terraform init`, in order to satisfy some plugin requirements. You can then run `terraform plan` to view the changes that will be performed. Finally to provision the infrastructure and deploy all of the needed containers, you must execute `terraform apply`.

Optionally, if you want to manage the cluster with `kubectl`, you should first run `gcloud auth login`, paste the link in the browser and copy the code show to the prompt in the terminal. You should then run `gcloud config set project agisit-2021-project-08a`.

Next, you should go to the Google Kubernetes Engine and on the cluster name - `boutique` - you should click on the three dots and on `Connect`. Copy that command and paste it on the VM.

Now, you should be able to manage the cluster using `kubectl`. Try running `kubectl get pods -n application` to view the running pods.

## Results

Now that the system is deployed, is time to test it. Go to the Google Kubernetes Engine and you should click on the cluster name - `boutique`. You will see a list of the running services, where you can access the IPs of the shop and the dashboard. 

Try to buy a product on the shop. You should be able to see a products list like this:

![](https://i.imgur.com/JK1X4EF.jpg)


Regarding the Grafana dashboard, the username is `admin` and the password is `password`. You will find a grafana dashboard that you can try on your own. Go to `Dashboards` > `Manage` and open `Istio Performance Dashboard`. You should see a something similiar to this:

![](https://i.imgur.com/gM2wgXf.png)

Explore other dashboards and try to see if you can understand what those different dashboards have and what do their values mean.
