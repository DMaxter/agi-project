# Project AGISIT 2021-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png) ![João Soares](../photos/jsoares.jpg) ![Tiago](../photos/ist189542-photo.png)

| Number | Name          | Username                                       | Email                                      |
| ------ | ------------- | ---------------------------------------------- | ------------------------------------------ |
| 89429  | Daniel Matos  | <https://git.rnl.tecnico.ulisboa.pt/ist189429> | <mailto:daniel.m.matos@tecnico.ulisboa.pt> |
| 89475  | João Soares   | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt>  |
| 89542  | Tiago Fonseca | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt> |

## Demo

To deploy the services you need to initialize `Terraform` with `terraform init`, in order to satisfy some plugin requirements. 

You can then run `terraform plan` to view the changes that will be performed. 

Finally to provision the infrastructure and deploy all of the needed containers, you must execute `terraform apply`.

Now that the deployment is done, we need to run `gcloud auth login` so that we can manage de cluster with `kubectl`.

A link showed up and you should paste it into the browser and copy the code showed to the prompt in the terminal. You should then run `gcloud config set project agisit-2021-project-08a`.

Next, go to the Google Kubernetes Engine and on the cluster name - `boutique` - you should click on the three dots and on `Connect`. Copy that command and paste it on the VM.

Now, we are able to manage the cluster using `kubectl`. Run `kubectl get pods -n application` in order to see the running pods.

Now that eveything is running ad expected, go to the Google Kubernetes Engine. Click on the cluster name - `boutique`. You will see a list of the running services, where you can access the IPs of the shop and the dashboard. 

First open the frontend link, that you redirect you to the shop. Buy a product from the list and see if you can finish your purchase.


With the shop working we can now finish our demo by checking the content od the Grafana. The username is `admin` and the password is `password`. Go to `Dashboards` > `Manage` and open `Istio Performance Dashboard`. You should see a something similiar to this:

![](https://i.imgur.com/gM2wgXf.png)


## Load Generator

We ran a load generator tool (provided by our application), which basically tries to buy a lot of stuff - like a lot of normal users would do -, against our deployment and the results can be seen in the following pictures:

![](https://i.imgur.com/EHThmhJ.png)
![](https://i.imgur.com/56TjseI.png)

You can do it as well in order to have your Grafana dashboards with more data that can actually be interpreted. To run our load generator, edit the `docker-compose.yml` file in `src/loadgenerator`. Change the variable `FRONTEND_ADDR` to the IP of the load balancer and then simply run `docker-compose up --build` so that it launches 9 services against the server.