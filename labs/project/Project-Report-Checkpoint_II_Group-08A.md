# Project Checkpoint II

AGISIT 2021-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png) ![João Soares](../photos/jsoares.jpg) ![Tiago](../photos/ist189542-photo.png)

| Number | Name          | Username                                       | Email                                      |
| ------ | ------------- | ---------------------------------------------- | ------------------------------------------ |
| 89429  | Daniel Matos  | <https://git.rnl.tecnico.ulisboa.pt/ist189429> | <mailto:daniel.m.matos@tecnico.ulisboa.pt> |
| 89475  | João Soares   | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt>  |
| 89542  | Tiago Fonseca | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt> |

## Module Leaders

| Module                                       | Leader        |
| -------------------------------------------- | ------------- |
| Architecture of the Solution                 | João Soares   |
| Configuration of the monitoring System       | Tiago Fonseca |
| Compute nodes capabilities                   | João Soares   |
| Networking and metric details                | Daniel Matos  |
| Provisioning of the Infrastructure           | Daniel Matos  |
| Scalability and High Availability Strategies | ------------- |
| Deployment of the Application                | Tiago Fonseca |

All other members of the group contributed in every single task, since we developed the project during the labs and in group sessions on discord.

## Pre-Requisites

To host our services, we chose to use the Google Cloud Platform (GCP). We are relying on Terraform, a tool to help us create and manage the infrastructure.

There should be a `terraform.tfvars` file that contains the following 3 variables (values are populated with our choices):

```tfvars
project = "agisit-2021-project-08a"
workers_count = "3"
region = "europe-west3-a"
```

Furthermore, we are using the monitoring tool Grafana. We have prepared a JSON file to import the dashboard with the chosen metrics.

The GCP project name is **AGISIT-2021-PROJECT-08A**.

## Project Files

```
project
├── id_rsa.pub (public key uploaded to the nodes)
├── pb (contains the proto files used in the source doe of the microservices)
├── src (contains the source code for all the microservices)
│   ├── adservice
│   ├── cartservice
│   ├── checkoutservice
│   ├── currencyservice
│   ├── emailservice
│   ├── frontend
│   ├── loadgenerator
│   ├── paymentservice
│   ├── productcatalogservice
│   ├── recommendationservice
│   ├── shippingservice
├── terraform
│   ├── agisit-2021-project-08a-097c56533224.json (credentials for gcp)
│   ├── gcp-gke-main.tf
│   ├── gcp-gke-provider.tf
│   ├── gke
│   │   └── gcp-gke-cluster.tf
│   ├── k8s
│   │   ├── k8s-ad.tf
│   │   ├── k8s-cart.tf
│   │   ├── k8s-checkout.tf
│   │   ├── k8s-currency.tf
│   │   ├── k8s-email.tf
│   │   ├── k8s-frontend.tf
│   │   ├── k8s-grafana.tf
│   │   ├── k8s-payment.tf
│   │   ├── k8s-productcatalog.tf
│   │   ├── k8s-provider.tf
│   │   ├── k8s-recommendation.tf
│   │   ├── k8s-services.tf
│   │   ├── k8s-shipping.tf
│   │   └── k8s-variables.tf
│   └── terraform.tfvars
```

## Deployment

To deploy the services you need to initialize `Terraform` with `terraform init`, in order to satisfy some plugin requirements. You can then run `terraform plan` to view the changes that will be performed. Finally to provision the infrastructure, you must execute `terraform apply`.

### Current deployment

Currently, we have everything containerized, so we decided to use Google Kubernetes Engine to deploy all our microservices. To simplify even more its management, everything is configured using only terraform, so the only command needed is `terraform apply`, as mentioned above.
We also deployed Grafana the same way, in order to make it simple and efficient.

### Future Work

Later we plan to improve our deployment by using Jenkins in order to test, package and deploy the application automatically.
