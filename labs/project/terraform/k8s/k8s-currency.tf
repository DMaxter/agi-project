resource "kubernetes_deployment" "currency" {
  metadata {
    name = "currency-service"
    labels = {
      app  = "boutique"
      tier = "currency-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "currency-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "currency-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/currencyservice:v0.3.0"
          name  = "currency-service"

          port {
            container_port = 7000
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

	  env {
            name  = "PORT"
	    value = "7000"
          }
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
