resource "kubernetes_deployment" "productcatalog" {
  metadata {
    name = "productcatalog-service"
    labels = {
      app  = "boutique"
      tier = "productcatalog-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "productcatalog-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "productcatalog-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/productcatalogservice:v0.3.0"
          name  = "productcatalog-service"

          port {
            container_port = 3550
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

	  env {
	    name  = "PORT"
            value = "3550"
	  }
        }
      }
    }
  }
  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
