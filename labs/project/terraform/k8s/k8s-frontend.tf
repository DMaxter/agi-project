resource "kubernetes_deployment" "frontend" {
  metadata {
    name = "frontend-service"
    labels = {
      app  = "boutique"
      tier = "frontend-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "frontend-service"
      }
    }

    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "frontend-service"
        }
      }

      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/frontend:v0.3.0"
          name  = "frontend-service"

          port {
            container_port = 8080
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

          env {
            name  = "PORT"
            value = "8080"
          }

          env {
            name  = "PRODUCT_CATALOG_SERVICE_ADDR"
            value = "productcatalog-service:3550"
          }

          env {
            name  = "CURRENCY_SERVICE_ADDR"
            value = "currency-service:7000"
          }

          env {
            name  = "CART_SERVICE_ADDR"
            value = "cart-service:7070"
          }

          env {
            name  = "RECOMMENDATION_SERVICE_ADDR"
            value = "recommendation-service:8080"
          }

          env {
            name  = "SHIPPING_SERVICE_ADDR"
            value = "shipping-service:50051"
          }

          env {
            name  = "CHECKOUT_SERVICE_ADDR"
            value = "checkout-service:5050"
          }

          env {
            name  = "AD_SERVICE_ADDR"
            value = "ad-service:5050"
          }
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
