resource "kubernetes_deployment" "ad" {
  metadata {
    name = "ad-service"
    labels = {
      app  = "boutique"
      tier = "ad-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "ad-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "ad-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/adservice:v0.3.0"
          name  = "ad-service"

          port {
            container_port = 9555
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

	  env {
	    name  = "PORT"
            value = "9555"
	  }
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
