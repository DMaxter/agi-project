resource "kubernetes_deployment" "shipping" {
  metadata {
    name = "shipping-service"
    labels = {
      app  = "boutique"
      tier = "shipping-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "shipping-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "shipping-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/shippingservice:v0.3.0"
          name  = "shipping-service"

          port {
            container_port = 50051
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

	  env {
	    name  = "PORT"
            value = "50051"
	  }
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
