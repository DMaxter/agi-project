resource "kubernetes_deployment" "email" {
  metadata {
    name = "email-service"
    labels = {
      app  = "boutique"
      tier = "email-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "email-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "email-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/emailservice:v0.3.0"
          name  = "email-service"

          port {
            container_port = 8080
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

	  env {
	    name  = "PORT"
            value = "8080"
	  }
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
