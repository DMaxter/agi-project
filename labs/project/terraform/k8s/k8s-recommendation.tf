resource "kubernetes_deployment" "recommendation" {
  metadata {
    name = "recommendation-service"
    labels = {
      app  = "boutique"
      tier = "recommendation-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "recommendation-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "recommendation-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/recommendationservice:v0.3.0"
          name  = "recommendation-service"

          port {
            container_port = 8080
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

	  env {
	    name  = "PORT"
            value = "8080"
	  }

          env {
            name  = "PRODUCT_CATALOG_SERVICE_ADDR"
            value = "productcatalog-service:3550"
          }
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
