resource "kubernetes_deployment" "checkout" {
  metadata {
    name = "checkout-service"
    labels = {
      app  = "boutique"
      tier = "checkout-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "checkout-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "checkout-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/checkoutservice:v0.3.0"
          name  = "checkout-service"

          port {
            container_port = 5050
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

          env {
            name  = "PORT"
            value = "5050"
          }

          env {
            name  = "PRODUCT_CATALOG_SERVICE_ADDR"
            value = "productcatalog-service:3550"
          }

          env {
            name  = "CURRENCY_SERVICE_ADDR"
            value = "currency-service:7000"
          }

          env {
            name  = "CART_SERVICE_ADDR"
            value = "cart-service:7070"
          }

          env {
            name  = "SHIPPING_SERVICE_ADDR"
            value = "shipping-service:50051"
          }

          env {
            name  = "EMAIL_SERVICE_ADDR"
            value = "email-service:8080"
          }

          env {
            name  = "PAYMENT_SERVICE_ADDR"
            value = "payment-service:50051"
          }
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
