resource "kubernetes_deployment" "cart" {
  metadata {
    name = "cart-service"
    labels = {
      app  = "boutique"
      tier = "cart-service"
    }
    namespace = kubernetes_namespace.application.id
  }

  spec {
    progress_deadline_seconds = 1200 # In case of taking longer than 9 minutes
    replicas = 1
    selector {
      match_labels = {
        app  = "boutique"
        tier = "cart-service"
      }
    }
    template {
      metadata {
        labels = {
          app  = "boutique"
          tier = "cart-service"
        }
      }
      spec {
        container {
          image = "gcr.io/google-samples/microservices-demo/cartservice:v0.3.0"
          name  = "cart-service"

          port {
            container_port = 7070
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "100Mi"
            }
          }

          env {
  	    name  = "REDIS_ADDR"
	    value = "localhost:6379"
          }
        }

        container {
	  image = "redis:alpine"
	  name  = "redis-cart"

          port {
	    container_port = 6379
          }

	  volume_mount {
	    mount_path = "/data"
	    name       = "redis-volume"
          }
        }

        volume {
          name = "redis-volume"
        }
      }
    }
  }

  depends_on = [
    helm_release.istiod,
    kubernetes_namespace.application
  ]
}
