#################################################################
# Definition of the Services
#################################################################
resource "kubernetes_service" "ad-service" {
  metadata {
    name = "ad-service"

    labels = {
      app  = "boutique"
      tier = "ad-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "ad-service"
    }

    port {
      port        = 9555
      target_port = 9555
    }
  }
}

resource "kubernetes_service" "cart-service" {
  metadata {
    name = "cart-service"

    labels = {
      app  = "boutique"
      tier = "cart-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "cart-service"
    }

    port {
      port        = 7070
      target_port = 7070
    }
  }
}

resource "kubernetes_service" "checkout-service" {
  metadata {
    name = "checkout-service"

    labels = {
      app  = "boutique"
      tier = "checkout-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "checkout-service"
    }

    port {
      port        = 5050
      target_port = 5050
    }
  }
}

resource "kubernetes_service" "currency-service" {
  metadata {
    name = "currency-service"

    labels = {
      app  = "boutique"
      tier = "currency-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "currency-service"
    }

    port {
      port        = 7000
      target_port = 7000
    }
  }
}

resource "kubernetes_service" "email-service" {
  metadata {
    name = "email-service"

    labels = {
      app  = "boutique"
      tier = "email-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "email-service"
    }

    port {
      port        = 8080
      target_port = 8080
    }
  }
}

resource "kubernetes_service" "frontend-service" {
  metadata {
    name = "frontend-service"

    labels = {
      app  = "boutique"
      tier = "frontend-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "frontend-service"
    }

    port {
      port        = 80
      target_port = 8080
    }
  }
}

resource "kubernetes_service" "payment-service" {
  metadata {
    name = "payment-service"

    labels = {
      app  = "boutique"
      tier = "payment-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "payment-service"
    }

    port {
      port        = 50051
      target_port = 50051
    }
  }
}

resource "kubernetes_service" "productcatalog-service" {
  metadata {
    name = "productcatalog-service"

    labels = {
      app  = "boutique"
      tier = "productcatalog-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "productcatalog-service"
    }

    port {
      port        = 3550
      target_port = 3550
    }
  }
}

resource "kubernetes_service" "recommendation-service" {
  metadata {
    name = "recommendation-service"

    labels = {
      app  = "boutique"
      tier = "recommendation-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "recommendation-service"
    }

    port {
      port        = 8080
      target_port = 8080
    }
  }
}

resource "kubernetes_service" "shipping-service" {
  metadata {
    name = "shipping-service"

    labels = {
      app   = "boutique"
      tier  = "shipping-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app   = "boutique"
      tier  = "shipping-service"
    }

    port {
      port        = 50051
      target_port = 50051
    }
  }
}

#################################################################
# The Service for the Frontend Load Balancer Ingress
resource "kubernetes_service" "boutique-lb" {
  metadata {
    name = "boutique-lb"

    labels = {
      app  = "boutique"
      tier = "frontend-service"
    }
    
    namespace = kubernetes_namespace.application.id    
  }

  spec {
    selector = {
      app  = "boutique"
      tier = "frontend-service"
    }

    type = "LoadBalancer"

    port {
      port        = 80
      target_port = 8080
    }
  }
}
