#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update
sudo apt-get -y upgrade

# Install all tools
sudo apt-get -y install software-properties-common unzip build-essential libssl-dev libffi-dev graphviz 

# Install Terraform
sudo apt-get update
# install GNU Privacy Guard
sudo apt-get install -y gnupg
# add HashiCorp GPG key
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update
sudo apt-get -y install terraform

# Install Google Cloud SDK
snap install google-cloud-sdk --classic

# Install Kubernetes Controller
sudo snap install kubectl --classic

# Clean up cached packages
sudo apt-get clean all

