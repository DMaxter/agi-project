# Lab-02 README

AGISIT 20201-2022

## Authors

**Team 08A**

![Tiago](../photos/ist189542-photo.png)


| Number | Name              | Username                                       | Email                                        |
| -------|-------------------|------------------------------------------------| ---------------------------------------------|
| 89542  | Tiago Fonseca     | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt>   |


# Provisioning the Infrastructure with Terraform

## Q01: When you run the command terraform init which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed two plugins. The first one was the plugin `openstack` to do X and the second one was the `random` plugin from hashicorp that X

![terraform init](pictures/89542_q1.png)

---

## Q02: Analyze briefly the terraform-vmcloud-servers.tf and interpret its purpose.

The main purpose of this file is to configure the `load balancer`, as well as the amount of desire `Web Servers` (line 27), as well as all the necessary configurations. For instance the `name` of the server, followed by the `image` and `flavor name` (lines 28, 29 and 30). There are some other configurations, like the `security group` configuration as well as the `network name`. Finally it also allows us to configure the information about the `key-pair` assigned to each server. All this information allows us to controll the way that the servers are created and deployed. 

---

## Q03: Analyze briefly the terraform-vmcloud-networks.tf and interpret its purpose.

The main purpose of this file is to allow access to servers through `HTTP` (line 9 to 14) and to `HTTPS` (line 16 to 21).
These two lines act as a security measures since they are controlling the remote access to the port `80` (HTTP) and `443` (HTTPS, pretty much like a firewall.

---

## Q04: If you would need more Web servers where and how would you declare that intention (with Terraform)?

I changed the count value on the line 27 of `terraform-vmcloud-servers.tf`, from 2 to 3 since I ran all the tests with 3 web servers.
To have more than 3 servers, we would have to change this value to a bigger number, and we would have to add new ips to the myhosts file (as well as the name of those servers below web and targets regions).

---

## Q05: Which other files appeared in the vmcloud folder after running terraform apply for your infrastructure? .

Before running terraform apply, there were the files present in the folder:

![before](pictures/89542_q5_1.png)


During the terraform apply, some changes were made:

![during](pictures/89542_q5_2.png)


And finally, after the terraform apply, these were the files present in the folder:

![after](pictures/89542_q5_3.png)


The file `terraform.tfstate` appeared after running terraform apply. After some analysis on the file, I understood that it is a file were all the terraform state is stored. After some more investigation on the subject, I learned that it is where terraform stores all the state about our managed infrastructure and configuration. This is also used whenever we want to create a plan and make changes on our infrastructure. 
The main is than to store a binding between objects in a remote system and resource instances that are declared in our configuration

---

## Q06: After creating the infrastructure with Terraform, you needed to modify the myhost inventory. What was changed in that file, and what was the purpose?

After running terraform apply, the infrastructure was created and deployed in the `VMCloud`. In order for Ansible to access the machines and configure them, I had to populate the `inventory` file, that in this case is the myhost file. I changed the IP values of all the servers and the load balancer (since they all were hardcoded as XXXXXXX) to the values given by the output of terraform.

This step is important for the `ansible` to be able to access and configure the remote machines. Having access to those IP's is a crucial part for it to work.  

---

# Deploy and Configure the Web Service with Ansible

## Q07: After creating the infrastructure with Terraform, you have tested the communication with the remote instances using an Ansible ad-hoc command. Copy the result into your report. Was the result successful? In case of positive answer, can you explain why it worked, i.e., how was Ansible capable of establishing a session with the remote instances?

The result is here

![communications](pictures/89542_q7.png)

For the simple ping using the ICMP protocol, I had to access the system `hosts`  file and added all the different IP's I got from the terraform apply. It allowed the system host to know all the new IP's and allows the ping to work.

For the Ansible ad-hoc "Ping-Pong", the answer to the previous question is one of the reasons why it worked. The other reason is the keypair we generated earlier on the laboratory guide. It allows us to securely connect via `ssh` withouth having to use a password. This keypair togheter with the population of the `inventory` file with the IP's from all the servers and load balancer, allowed ansible to contact them due to the IP provided from the terraform apply output.

---

## Q08: Which files did you had to modify, so that Ansible could Configure correctly the instances? Explain in detail what were the modifications.

I had an error on the `Deploy WebSite Content` task. The error was happening due to an unidentified variable.

I had a look on the files that were being used by that play, and there were not much variables that could be causing the issue.
After some minutes looking at it, and comparing those files with the files from the previous Lab, I found out that the `haproxy.cfg.j2` and the `index.html.j2` files had some problems with the usage of the `'` character (quotation).

The `haproxy.cfg.j2` had two `'` on the line 56 that were not necessary, since `ansible_default_ipv4.interface` is supposed to be a variable and it should not be inside quotations.
The `index.html.j2` had two missing `'` on the line 19. In this case the `ansible_facts` was missing the quotation in order for it to be able to access the correct address.


After re-running the playbook, now it was time for the `nginx` to give some errors. I had to comment the line 6 of the `index.html.j2` since we are not using an SSL certificate, meaning that we cannot listen to ssl connections on the port 443.

This time everything worked, and the instances were correctly configured.

---

## Q09: When the system was fully deployed, when hitting the refresh button on the web browser with the address of the Load Balancer, What changed? Describe those changes and interpret why they happened (if indeed happened).

This is an example of what happened when I pressed the refresh button:

Before refresh                             |  After refresh
:-----------------------------------------:|:------------------------------------------:
![before-refresh](pictures/89542_q9_1.png) | ![after-refresh](pictures/89542_q9_2.png)

Everytime we hit the refresh button, the browser changed to a different server. This is because of the Load Balancer, that is distributing the traffic across all the available servers. It is also the same behavior of the previous lab, which means that everyting worked, but this time using Terraform with Ansible.

---

# Destroying the Infrastructure

## Q10: When you run terraform destroy what was the result after you have confirmed the question "Do you really want to destroy all resources?". Copy those results to your report. After the destroy, looking at the VMCloud Dashboard, was there some resources left or the ones created have really been destroyed?

The result is here

![destroy 1](pictures/89542_q10_1.png)

![destroy 2](pictures/89542_q10_2.png)

![destroy 3](pictures/89542_q10_3.png)

![destroy 4](pictures/89542_q10_4.png)

![destroy 5](pictures/89542_q10_5.png)

![destroy 6](pictures/89542_q10_6.png)

All the values stored were changed to null, and at the end all the resources were destroyed.


The VMCloud Dashboard had the following result

Before destroy                             |  After destroy
:-----------------------------------------:|:------------------------------------------:
![before-destroy](pictures/89542_q10_7.png) | ![after-destroy](pictures/89542_q10_8.png)

Looking at the `VMCloud`, the only resources left were the ones that were already there before the laboratory guide steps (security group, router, ports, etc ..), so it confirms that the resources were destroyed successfully and everything worked as expected.



