# Lab 02 Report - ist189475

AGISIT 20201-2022

## Authors

**Team 08A - João Soares**

![João Soares](../photos/jsoares.jpg)

| Number | Name        | Username                                       | Email                                     |
| ------ | ----------- | ---------------------------------------------- | ----------------------------------------- |
| 89475  | João Soares | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt> |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command `terraform init` which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed 2 plugins: the `terraform-provider-openstack` and hashicorp's `random` module. The output of the command can be seen below:

![Output of `terraform init`](pictures/89475_q1.png)

### Q02: Analyze briefly the `terraform-vmcloud-servers.tf` and interpret its purpose.

This file contains the configurations for the resources to be deployed. Since terraform is declarative, the contents only state the desired configuration for the whole system, in this case regarding the servers.

We start by using the `random_string` provider to create a random name for the keypair that is going to be used on the servers, which is also declared just below it. This key is the one that we created on the `mgmt` node and was passed to the `terraform.tfvars` file.

Following these resources, we declare the web server configuration, using the `openstack_compute_instance_v2` provider. Here we choose the amount of web servers we want. In our case, we chose to go with 3. We also have a unique name per webserver usinga terraform variable resulting from the current count index. All web servers share the remaining settings: image, flavor, keypair and security groups. Finally they're also all placed within the same network.

The last resource we create is the load balancer, of same type as the servers. While most settings are the same, the notable difference is the lack of the `count` parameter - since we only want one load balancer - and the flavor is also different, since the balancer is gonna have to deal with more load. The network is also the same.

### Q03: Analyze briefly the `terraform-vmcloud-networks.tf` and interpret its purpose.

This file contains the configuration for the network deployed and used by all the nodes in the system. To be precise, it states that there should be a security group with two security rules, both targeting the TCP protocol: one for HTTP (port 80) and another for HTTPS (port 443).

### Q04: If you would need more Web servers where and how would you declare that intention (with Terraform)?

The `terraform-vmcloud-servers.tf` file was populated with 2 web servers by default. However we chose to increase this amount to 3 for our work. In case we would require more web servers we would simply increase this value to our desired number.

We would also have to update the `myhosts` file with more web server entries and their corresponding ips.

### Q05: Which other files appeared in the `vmcloud` folder after running `terraform apply` for your infrastructure?

Upon running the specified command, a new file was created: `terraform.tfstate`. This file contains all the information about the current state of the deployed infrastructure as well as all the defined outputs in each terraform configuration file. In case it is not the first time that the command is run, a backup file `terraform.tfstate.backup` is created as well.

### Q06: After creating the infrastructure with Terraform, you needed to modify the `myhosts` inventory. What was changed in that file, and what was the purpose?

Since the web servers were just created, there was no way of knowing which ip each node would have. This is a crucial piece of information for ansible to work properly. For that reason, we populated this file with the correct ips outputed by terraform, so that ansible can now which machines to connect to.

## Deploy and Configure the Web Service with Ansible

### Q07: After creating the infrastructure with Terraform, you have tested the communication with the remote instances using an Ansible ad-hoc command. Copy the result into your report. Was the result successful? In case of positive answer, can you explain why it worked, i.e., how was Ansible capable of establishing a session with the remote instances?

The ping command was successful. This is due to the fact that ansible not only knew the ips of the servers to connect to, which were edited in the `myhosts` file, but also because of the keypair specified in the terraform web server configuration. This section included the public key that we generated earlier on in the guide which allows us to connect securely via `ssh` without prompting for a password. Since ansible is **agentless** and connects via `ssh`, this was all that was required to achieve a successful connection with the remote instances.

![Output of `ansible ping`](pictures/89475_q7.png)

### Q08: Which files did you had to modify, so that Ansible could Configure correctly the instances? Explain in detail what were the modifications.

Initially, the `myhosts` file had to be updated with the correct number of instances as well as the corresponding ip addresses. The `terraform-vmcloud-servers.tf` file was also updated with 1 more web server. However these changes were performed and described earlier.

The remaining files that needed to be changed were 3 of the template files ansible was populating with variables and copying to the target machines.

For the web servers, we had to change the nginx `default-site.j2` file. Since we don't have an SSL certificate, we had to simple comment the line that made use of it - `listen 443 ssl;`. We also had to change variable that was being displayed on the webpage. The file `index.html.j2` was accessing the wrong variable, in fact it was trying to access a non-existing index: `ansible_facts`. By removing that and adding the correct prefix `ansible_` before the interface name, the ansible playbook finally managed to execute flawlessly. The resulting line was: `<p>({{ hostvars[ansible_hostname]["ansible_" + ansible_default_ipv4.interface].ipv4.address }})</p>`.

On the balancer's file, the `haproxy.cfg.j2`, we had to do something similar. The resulting line was: `server {{ host }} {{ hostvars[host]['ansible_' + ansible_default_ipv4.interface]['ipv4']['address'] }}:80 check`.

### Q09: When the system was fully deployed, when hitting the refresh button on the web browser with the address of the Load Balancer, What changed? Describe those changes and interpret why they happened (if indeed happened).

Since we're acessing the web servers indirectly via the load balancer, our request can end up in any of the 3 deployed web servers. When ansible copied the `index.html` file that is served by the web node when we access the website, it populated with the requested variables, namely the hostname and the ip address. This means that whenever we hit refresh, we can visibly see which web node we are being forwarded to.

## Destroying the Infrastructure

### Q10: When you run `terraform destroy` what was the result after you have confirmed the question "Do you really want to destroy all resources?". Copy those results to your report. After the destroy, looking at the VMCloud Dashboard, was there some resources left or the ones created have really been destroyed?

Upon running `terraform destroy`, the output was the following:

![Output of `terraform destroy`](pictures/89475_q10.png)

Looking at the VMCloud Dashboard, there were no resources left from the entire deployment: terraform did in fact destroy all of the infrastructure it had created and provisioned beforehand.
