# LAB 02 Report - ist189429

AGISIT 20201-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png)


| Number | Name              | Username                                     | Email                               |
| -------|-------------------|----------------------------------------------| ------------------------------------|
| 89429  | Daniel Matos      | https://git.rnl.tecnico.ulisboa.pt/ist189429 | mailto:daniel.m.matos@tecnico.ulisboa.pt   |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command `terraform init` which plugins were installed? (You can copy the result of the command to insert in your report).

When I ran the command `terraform init`, it installed the plugin `openstack` to interact with VMCloud, which uses OpenStack for managing their infrastructure.
It also installs the `random` plugin that is used to generate a random name for a keypair to be used to access the created machines.

The output of the command is:
```
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of terraform-provider-openstack/openstack...
- Finding latest version of hashicorp/random...
- Installing terraform-provider-openstack/openstack v1.44.0...
- Installed terraform-provider-openstack/openstack v1.44.0 (self-signed, key ID 4F80527A391BEFD2)
- Installing hashicorp/random v3.1.0...
- Installed hashicorp/random v3.1.0 (signed by HashiCorp)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

### Q02: Analyze briefly the `terraform-vmcloud-servers.tf` and interpret its purpose.

The file `terraform-vmcloud-servers.tf` is where I defined the machines I want to use.
In this case, we define the keypair to be used, what type of instance is the balancer, with what image and security group, as well as the network where it belongs. It also says the same thing for the web intances, along with the number of web servers to launch.

### Q03: Analyze briefly the `terraform-vmcloud-networks.tf` and interpret its purpose.

The file `terraform-vmcloud-networks.tf` is where I defined the security group, which connections are allowed, in which protocol and to what addresses. The security group acts like a firewall for the cloud.

### Q04: If you would need more Web servers where and how would you declare that intention (with Terraform)?

If I would need more server, I would just go to the file `terraform-vmcloud-servers.tf` and edit the number to the desired amount, by changing the value of `count`.

### Q05: Which other files appeared in the vmcloud folder after running `terraform apply` for your infrastructure?

After running `terraform apply`, a new file named `terraform.tfstate` where the state about the deployed infrastructure and configuration is stored.

### Q06: After creating the infrastructure with Terraform, you needed to modify the `myhosts` inventory. What was changed in that file, and what was the purpose?

I needed to set the IPs of the `balancer` and of the multiple `web` instances in the `myhosts` file, so that ansible could know where the machines were located.


## Deploy and Configure the Web Service with Ansible

### Q07: After creating the infrastructure with Terraform, you have tested the communication with the remote instances using an Ansible ad-hoc command. **Copy the result into your report. Was the result successful? In case of positive answer, can you explain why it worked, i.e., how was Ansible capable of establishing a session with the remote instances?**

To test the communication with the remote instances I used the command `ansible targets -m ping` and it was successful.
This happens because I have set the location of our public key in the `terraform.tfvars` file, so when the instances were deployed, our key was automatically added to the `authorized_keys` file of each instance, so the command ran successfully.

The output was:

```
$ ansible targets -m ping
web2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
balancer | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
web1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
web3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

### Q08: Which files did you had to modify, so that Ansible could Configure correctly the instances? Explain in detail what were the modifications.

To correctly configure the instances, I had to changes in 2 different files in the `templates` folder:

- Line 19 in `index.html.j2`: in `{{ hostvars[ansible_hostname][ansible_facts][ansible_default_ipv4.interface].ipv4.address }}`, `ansible_facts` needed single quotes around, so the line in the end became: `{{ hostvars[ansible_hostname]['ansible_facts'][ansible_default_ipv4.interface].ipv4.address }}`

- Line 56 in `haproxy.cfg.j2`: in `{{ hostvars[host]['ansible_facts']['ansible_default_ipv4.interface']['ipv4']['address'] }}`, there were extra some extra single quotes surrounding `ansible_default_ipv4.interface`, so in the end, it became: `hostvars[host]['ansible_facts'][ansible_default_ipv4.interface]['ipv4']['address'] }}`

### Q09: When the system was fully deployed, when hitting the refresh button on the web browser with the address of the Load Balancer, **What changed?** Describe those changes and interpret why they happened (if indeed happened).

When the web page was refreshed, a different IP and hostname were shown each time, in a round-robin way, going from web1 to web3. This happens due to the load balancing provided by the HA Proxy running in the `balancer` instance.


## Destroying the Infrastructure

### Q10: When you run `terraform destroy` what was the result after you have confirmed the question "Do you really want to destroy all resources?". Copy those results to your report. After the destroy, looking at the VMCloud Dashboard, was there some resources left or the ones created have really been destroyed?

When I ran `terraform destroy`, it destroyed the resources it had allocated previously.
However, when looking at the VMCloud Dashboard, there are still some resources allocated, but those have already been allocated beforehand - a security group, a router, some ports, a network and some security rules -, not by me, so in fact, Terraform only destroyed what it had created, nothing more and nothing less.

The final output of this command was:

```
openstack_compute_instance_v2.balancer: Destroying... [id=39c54228-eb59-4a75-a98f-e959b3a4389c]
openstack_compute_instance_v2.web[0]: Destroying... [id=bf1fd30a-23d9-4652-a9a7-b473f28f2d43]
openstack_compute_instance_v2.web[2]: Destroying... [id=fde74275-9b74-42bc-926a-e819df577427]
openstack_compute_instance_v2.web[1]: Destroying... [id=a1d14dd2-42c9-4708-a48c-a17049cbb901]
openstack_compute_instance_v2.balancer: Still destroying... [id=39c54228-eb59-4a75-a98f-e959b3a4389c, 10s elapsed]
openstack_compute_instance_v2.web[0]: Still destroying... [id=bf1fd30a-23d9-4652-a9a7-b473f28f2d43, 10s elapsed]
openstack_compute_instance_v2.web[1]: Still destroying... [id=a1d14dd2-42c9-4708-a48c-a17049cbb901, 10s elapsed]
openstack_compute_instance_v2.web[2]: Still destroying... [id=fde74275-9b74-42bc-926a-e819df577427, 10s elapsed]
openstack_compute_instance_v2.web[2]: Destruction complete after 11s
openstack_compute_instance_v2.web[1]: Destruction complete after 11s
openstack_compute_instance_v2.web[0]: Destruction complete after 12s
openstack_compute_instance_v2.balancer: Destruction complete after 12s
openstack_compute_secgroup_v2.sec_ingr: Destroying... [id=ffe234bc-fa05-464f-bf0f-303df3e89554]
openstack_compute_keypair_v2.keypair: Destroying... [id=x977]
openstack_compute_keypair_v2.keypair: Destruction complete after 0s
random_string.random_name: Destroying... [id=x977]
random_string.random_name: Destruction complete after 0s
openstack_compute_secgroup_v2.sec_ingr: Destruction complete after 1s
```
