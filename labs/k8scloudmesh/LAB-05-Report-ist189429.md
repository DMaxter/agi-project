# LAB 04 Report - ist189429

AGISIT 20201-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png)


| Number | Name              | Username                                     | Email                               |
| -------|-------------------|----------------------------------------------| ------------------------------------|
| 89429  | Daniel Matos      | https://git.rnl.tecnico.ulisboa.pt/ist189429 | mailto:daniel.m.matos@tecnico.ulisboa.pt   |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command `terraform init` which plugins were installed? (You can copy the result of the command to insert in your report).

When I ran the command `terraform init`, it installed 4 plugins:

- `google` - so Terraform could interact with Google Cloud Platform and manage the infrastructure there
- `kubernetes` - to manage the Kubernetes infrastructure, namely the cluster and the Kubernetes resources
- `helm` - to deploy software packages to Kubernetes, using Terraform 
- `kubectl` - to handle raw Kubernetes manifest yaml files

The output of the command is:

```
$ terraform init
Initializing modules...
- gcp_gke in gcp_gke
- gcp_k8s in gcp_k8s

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/kubernetes...
- Finding latest version of hashicorp/google...
- Finding hashicorp/helm versions matching "2.3.0"...
- Finding gavinbunney/kubectl versions matching "1.13.0"...
- Installing hashicorp/google v4.0.0...
- Installed hashicorp/google v4.0.0 (signed by HashiCorp)
- Installing hashicorp/helm v2.3.0...
- Installed hashicorp/helm v2.3.0 (signed by HashiCorp)
- Installing gavinbunney/kubectl v1.13.0...
- Installed gavinbunney/kubectl v1.13.0 (self-signed, key ID AD64217B5ADD572F)
- Installing hashicorp/kubernetes v2.6.1...
- Installed hashicorp/kubernetes v2.6.1 (signed by HashiCorp)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

### Q02: Analyze briefly the `k8s-monitoring.tf` and interpret its purpose.

The file `k8s-monitoring.tf` defines where the files containing the Kubernetes configuration of Prometheus and Grafana are located, as well as the dependencies of each manifest file.

### Q03: Analyze briefly the `k8s-istio.tf` and interpret its purpose.

The file `k8s-istio.tf` defines where helm should get the Istio service to install it and run it in my Kubernetes cluster.

### Q04: In Module `gcp_k8s` there is a folder named `monitoring` containing two `.yaml` files. What is the purpose of those files?

In the `monitoring` folder there are 2 files:
- `grafana.yaml` - which defines how Grafana should be configured in Kubernetes, namely how to get its image, how it connects to Prometheus, what graphs should be present and where they should be mounted, as well as port and environment variables configurations.
- `prometheus.yaml` - which defines the configuration of Prometheus in my Kubernetes cluster and how it can be accessed.

### Q05: Use the command `kubectl get pods` to get the information about the `application` Pods and report the result. How many containers were reported in each Pod?

In each pod, 2 containers are reported, due to Istio running inside the cluster and injecting a sidecar container to each pod.

### Q06: Check all the information in the `istio-system` and report the result. Which Pods in state `running` were reported. Are there `replica sets` reported?

The pods that are in state `running` that were reported, are the ones corresponding to Grafana, Prometheus and Istiod.

There are 3 replica sets reported by this command: grafana, prometheus and istiod.

The output of the command is:

```
$ kubectl get all -n istio-system
NAME                             READY   STATUS    RESTARTS   AGE
pod/grafana-79bd5c4498-qvn4k     1/1     Running   0          18m
pod/istiod-687f965684-j8ckg      1/1     Running   0          12m
pod/prometheus-9f4947649-htvz6   2/2     Running   0          18m

NAME                 TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                                 AGE
service/grafana      LoadBalancer   10.3.244.11    104.155.83.41   3000:31857/TCP                          18m
service/istiod       ClusterIP      10.3.254.250   <none>          15010/TCP,15012/TCP,443/TCP,15014/TCP   12m
service/prometheus   LoadBalancer   10.3.240.113   34.140.21.124   9090:30610/TCP                          18m

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/grafana      1/1     1            1           18m
deployment.apps/istiod       1/1     1            1           12m
deployment.apps/prometheus   1/1     1            1           18m

NAME                                   DESIRED   CURRENT   READY   AGE
replicaset.apps/grafana-79bd5c4498     1         1         1       18m
replicaset.apps/istiod-687f965684      1         1         1       12m
replicaset.apps/prometheus-9f4947649   1         1         1       18m

NAME                                         REFERENCE           TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/istiod   Deployment/istiod   1%/80%    1         5         1          12m
```


### Q07: Get the information about all the services running in the Kubernetes cluster, and write it in the report.

We can obtain information of all the services running in the Kubernetes cluster with the command `kubectl get services --all-namespaces`, and it's output is:

```
$ kubectl get service --all-namespaces
NAMESPACE      NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                                 AGE
application    frontend               LoadBalancer   10.3.248.212   34.79.215.246   80:30445/TCP                            29m
application    redis-follower         ClusterIP      10.3.243.60    <none>          6379/TCP                                29m
application    redis-leader           ClusterIP      10.3.244.158   <none>          6379/TCP                                29m
default        kubernetes             ClusterIP      10.3.240.1     <none>          443/TCP                                 30m
istio-system   grafana                LoadBalancer   10.3.244.11    104.155.83.41   3000:31857/TCP                          29m
istio-system   istiod                 ClusterIP      10.3.254.250   <none>          15010/TCP,15012/TCP,443/TCP,15014/TCP   23m
istio-system   prometheus             LoadBalancer   10.3.240.113   34.140.21.124   9090:30610/TCP                          29m
kube-system    default-http-backend   NodePort       10.3.241.23    <none>          80:30669/TCP                            30m
kube-system    kube-dns               ClusterIP      10.3.240.10    <none>          53/UDP,53/TCP                           30m
kube-system    metrics-server         ClusterIP      10.3.254.68    <none>          443/TCP                                 30m
```

### Q08: Which metrics (and targets) have you selected to observe in Prometheus? Post a screenshot of the Prometheus Dashboard showing the values of some of the metrics for `kubernetes-pods`.

In Prometheus, I observed the number of running pods in each of the nodes.

![Prometheus](images/89429_q8.png)

### Q09: Select a Grafana Performance Dashboard. Which Metrics compose the Dashboard? Post a screenshot of that Dashboard in your report.

The Grafana Performance Dashboard is composed of the vCPU and memory usage metrics, as well as the network transfers that occur in the Istio components.

![Grafana](images/89429_q9.png)

### Q10: Select a Grafana Service Dashboard. Which Metrics compose the Dashboard? Post a screenshot of that Dashboard in your report.

The Grafana Service Dashboard is composed of the throughput at the client and server site, the latency and the network velocity. It also contains the success rate of the requests made and their size.

![Grafana](images/89429_q10.png)
