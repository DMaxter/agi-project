# Lab-05 README

AGISIT 20201-2022

## Authors

**Team 08A**

![Tiago](../photos/ist189542-photo.png)


| Number | Name              | Username                                       | Email                                        |
| -------|-------------------|------------------------------------------------| ---------------------------------------------|
| 89542  | Tiago Fonseca     | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt>   |


# Provisioning the Infrastructure with Terraform

## Q01: When you run the command terraform init which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed four plugins:

- `gavingunney/kubectl`, that is a provider to manage Kubernetes resources in Terraform, while allowing yaml.
- `hashicorp/google`, that is the Google provider used to configure Google Cloud Platform infrastructure .
- `hashicorp/kubernetes`, that allows for the management of all kubernetes resources.
- `hashicorp/helm`, that is a provider used to deploy software packages in Kubernetes.

![terraform init](pictures/89542_q1.png)

---

## Q02: Analyze briefly the k8s-monitoring.tf and interpret its purpose.

The file `k8s-monitoring.tf` defines where the `yaml` files of both `Prometheus` and `Grafana` are located. It also defines the dependencies of their manifests, that depend on the `kubernetes_namespace.istio_system`.

---

## Q03: Analyze briefly the k8s-istio.tf and interpret its purpose.

The file `k8s-istio.tf` is where the `Istio service` is installed by helm and deployed. As in the previous questions, this file also defines the dependencies needed, that in this case also have the var `cluster` and the `helm_release.istio_base` value.

---

## Q04: In Module gcp_k8s there is a folder named monitoring containing two .ymlfiles. What is the purpose of those files?

The `monitoring` folder contains the following files:

- `grafana.yml` - defines the configuration of the `Grafana` in the cluster. There is configured how to get the docker grafana image (grafana/grafana:8.1.2), as well as the ports and env variables needed. Finally, and very important, there is the connection to the Prometheus service (http://prometheus:9090) that will be providing all the metrics to Grafana. All the dashboard structures are also configured in this file.

- `prometheus.yaml` - defines the configuration of the `Prometheus` in the cluster. There is configured how to get the docker prometheus image (jimmidyson/configmap-reload:v0.5.0), as well as the ports needed. 

---

## Q05: Use the command kubectl get pods to get the information about the application Pods and report the result. How many containers were reported in each Pod?

Each Pod has more than one container running as we can see in the value of `(READY: 2/2)`. This is due to the label added in the namespace creation, corresponding to the injected sidecar container of the Istio Service Mesh. 

![kubectl get pods](pictures/89542_q5.png)

---

## Q06: Check all the information in the istio-system and report the result. Which Pods in state running were reported. Are there replica sets reported?

The reported pods are the ones corresponding to `Grafana`, `Istiod` and `Prometheus`.

The replica sets reported were also the ones corresponding to `Grafana`, `Istiod` and `Prometheus`, that is 3 replica sets reported.

![kubectl get all istio-system](pictures/89542_q6.png)

---

## Q07: Get the information about all the services running in the Kubernetes cluster, and write it in the report.

At first I run the command `kubectl get service` and got this output:

![kubectl get service](pictures/89542_q7_1.png)

It returned the cluster information, that is associated with the `default` namespace. In order to access all the services, I had to add a flag to the command. So I run `kubectl get service --all-namespaces` and got this output:

![kubectl get service --all-namespaces](pictures/89542_q7_2.png)

---

## Q08: Which metrics (and targets) have you selected to observe in Prometheus? Post a screenshot of the Prometheus Dashboard showing the values of some of the metrics for kubernetes-pods.

The values os the prometheus targets for the kubernetes-pods were the following:

![prometheus targets](pictures/89542_q8_1.png)

I also looked at the Graph tab, and looked for some metrics about the pods that were running. After some filtering, I ended up with this cool graph:

![prometheus graph](pictures/89542_q8_2.png)

---

## Q09: Select a Grafana Performance Dashboard. Which Metrics compose the Dashboard? Post a screenshot of that Dashboard in your report?

The Performance dashboard is the following:

![grafana performance vCPU and memory Usage](pictures/89542_q9_1.png)

![grafana performance Istio](pictures/89542_q9_2.png)

We can see metrics regarding the `vCPU` usage that shows the utilization by the main Istio components as well as the number of `Goroutines`. We can also see metrics about the `Memory` of each component.

---

## Q10: Select a Grafana Service Dashboard. Which Metrics compose the Dashboard? Post a screenshot of that Dashboard in your report?

The Service Dashboard is divided in 3 sections:

- the general section has some metrics regarding the `Success Rate` of the `Client` and `Server` Requests, as well as the `Received` and `Sent` Bytes.

![grafana service general](pictures/89542_q10_1.png)

- the client-workloads section has more detailed information about the client requests.

![grafana service client-workloads](pictures/89542_q10_2.png)

- the service-workloads section has more detailed information about the server requests.

![grafana service service-workloads](pictures/89542_q10_3.png)
