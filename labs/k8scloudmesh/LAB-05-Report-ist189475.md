# Lab 05 Report - ist189475

AGISIT 2021-2022

## Authors

**Team 08A - João Soares**

![João Soares](../photos/jsoares.jpg)

| Number | Name        | Username                                       | Email                                     |
| ------ | ----------- | ---------------------------------------------- | ----------------------------------------- |
| 89475  | João Soares | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt> |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command `terraform init` which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed 4 plugins: hashicorp's `google`, `kubernetes` and `helm` modules as well as gavinbunney's `kubectl` module. The output of the command can be seen below:

![Output of `terraform init`](pictures/89475_q1.png)

### Q02: Analyze briefly the `k8s-monitoring.tf` and interpret its purpose.

This file is responsible for spinning up the monitoring tools: `grafana` and `prometheus`. It makes use of the `kubectl` module to interact with the previously deployed kubernetes cluster.
It starts by loading the `prometheus` yaml manifest file. This file contains the kubernetes configuration for this deployment (pods, services, roles, service accounts, etc). It also contains the deployment specific configuration (json). The yaml file is actually composed of multiple documents, separated by the three dashes (`---`). These "subdocuments" are then loaded and executed individually by the next `kubectl module` from the `k8s-monitoring.tf` file. It does this for the `grafana.yaml` file as well.

### Q03: Analyze briefly the `k8s-istio.tf` and interpret its purpose.

This file is responsible for deploying all the kubernetes configuration for the istio system. It uses the `helm` module (kubernetes package manager) to deploy all the necessary infrastructure (pods, services, roles, service accounts, etc). All of these systems are deployed under the `istio namespace` defined in `k8s-namespaces.tf`.

### Q04: In Module `gcp_k8s` there is a folder named `monitoring` containing two `.yml` files. What is the purpose of those files?

These files contain the kubernetes configuration for the corresponding deployments (pods, services, roles, service accounts, etc). They also contain the application configurations (json). Each yaml file is actually composed of multiple documents, separated by the three dashes (`---`). These documents are then loaded and executed individually by the `kubectl` terraform module, which provisions and deploys the defined infrastructure.

### Q05: Use the command `kubectl get pods` to get the information about the `application` pods and report the result. How many containers were reported in each Pod?

The output of the command `kubectl get pods -n application` is the following:

![Output of `kubectl get pods -n application`](pictures/89475_q5.png)

From the output, we can see that each pod has 2 containers.

### Q06: Check all the information in the `istio-system` and report the result. Which Pods in state `running` were reported. Are there `replica sets` reported?

The output of the command `kubectl get all -n istio-system` is the following:

![Output of `kubectl get all -n istio-system`](pictures/89475_q6.png)

From the output, we know that the deployed pods are the `grafana` pod, the `istiod` pod and the `prometheus` pod (actually 2 pods). There is also a service for each these modules, with only the `grafana` and the `prometheus` services being LoadBalancers (with external IP addresses).

Furthermore, there are 3 `replica sets` reported, one for each app (`grafana`, `istiod`, `prometheus`).

Finally there is an autoscaler in place referencing the `istiod` deployment.

### Q07: Get the information about all the services running in the Kubernetes cluster, and write it in the report.

The output of the command `kubectl get svc --all-namespaces` is the following:

![Output of `kubectl get svc --all-namespaces`](pictures/89475_q7.png)

### Q08: Which metrics (and targets) have you selected to observe in Prometheus? Post a screenshot of the Prometheus Dashboard showing the values of some of the metrics for `kubernetes-pods`.

The Prometheus Targets Dashboard (selected `kubernetes pods`) showed the following data:
![Output of Prometheus Targets Dashboard (A)](pictures/89475_q8_a.png)
![Output of Prometheus Targets Dashboard (B)](pictures/89475_q8_b.png)

### Q09: Select a Grafana Performance Dashboard. Which Metrics compose the Dashboard? Post a screenshot of that Dashboard in your report?

The Grafana Performance Dashboard showed the following data:
![Output of Grafana Performance Dashboard (A)](pictures/89475_q9_a.png)
![Output of Grafana Performance Dashboard (B)](pictures/89475_q9_b.png)
![Output of Grafana Performance Dashboard (C)](pictures/89475_q9_c.png)

The metrics displayed in this dashboard are the following:

- **vCPU / 1k rps**: shows the vCPU utilization per 1k (1000) requests per second. In the screenshot, this graph is blank because the service did not receive enough load to populate it.
- **vCPU**: shows the vCPU utilization for each istio component (this is not normalized)
- **Memory Usage**: shows the memory footprint of each component. In the screenshot, only the istio-proxy has enough data to show up in the graph (the remaning components are normalized by 1k (1000) requests per second)
- **Bytes transferred / sec**: shows the number of bytes that flow through each istio component. In the screenshot no data is displayed.

### Q10: Select a Grafana Service Dashboard. Which Metrics compose the Dashboard? Post a screenshot of that Dashboard in your report?

The Grafana Service Dashboard showed the following data:
![Output of Grafana Service Dashboard (A)](pictures/89475_q10_a.png)
![Output of Grafana Service Dashboard (B)](pictures/89475_q10_b.png)
![Output of Grafana Service Dashboard (C)](pictures/89475_q10_c.png)

This dashboard is divided into 3 panels: **General**, **Client Workloads**, **Service Workloads**.
Each panel has its own set of metrics:

**General:**
This panel displays metrics for the service `redis-leader.application.svc.cluster.local`

- **Client Request Volume**
- **Client Success Rate**
- **Server Request Volume**
- **Server Success Rate**
- **Client Request Duration**
- **Server Request Duration**
- **TCP Received Bytes**
- **TCP Sent Bytes**

**Client Workloads:**

- **Incoming Requests by Source and Response Code**
- **Incoming Success Rate by Source**
- **Incoming Request Duration by Source**
- **Incoming Request Size by Source**
- **Response Size by Source**
- **Bytes Received from Incoming TCP Connection**
- **Bytes Sent to Incoming TCP Connection**

**Service Workloads:**

- **Incoming Requests by Destination Workload and Response Code**
- **Incoming Success Rate by Destination Workload**
- **Incoming Request Duration by Service Workload**
- **Incoming Request Size by Service Workload**
- **Response Size by Service Workload**
- **Bytes Received from Incoming TCP Connection**
- **Bytes Sent to Incoming TCP Connection**

_NOTE:_ The **Client Workloads** and **Service Workloads** metrics are similar, the difference is the sorting metric.
