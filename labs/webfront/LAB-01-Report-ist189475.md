# Lab 01 Report - ist189475

AGISIT 20201-2022

## Authors

**Team 08A - João Soares**

![João Soares](../photos/jsoares.jpg)

| Number | Name        | Username                                       | Email                                     |
| ------ | ----------- | ---------------------------------------------- | ----------------------------------------- |
| 89475  | João Soares | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt> |

## The Vagrant Environment

### Q01: Interpret the Vagranfile that will be used, explaining, in your own words, what you think the “instructions” in it are supposed to do.

The Vagrantfile that we're using is the one located inside `labs/webfront`. This ruby file contains information about the virtual machines that we're launching to use during the lab. In my case, I'm using the virtual box version, which means it needs to set it as the default provider, as well as install some essential plugins.

After that, the file describes the virtual machines, like a recipe book. After defining some global configuration parameters, we create a Management (mgmt) VM, which will be used to interact with the remainder of the cluster. The template is similar even amongst the different nodes. We start by defining the name, the image (ubuntu/focal64 for all of them), network settings (hostname and a static ip for each vm) as well as the virtual box provider settings (memory & cpu). We also share the tools folder with the nodes, so that we can access its files within the vm environment.
This is the same for the rest of the nodes: the balancer and the 4 web servers (we use a loop to create these). There is but one major difference between all the nodes, which is the provisioning of the vms (configuration of the system and installation of packages). All nodes run the `hosts_ssh.sh` script, but only the `mgmt` box executes the remaining 2, `hosts_ip.ssh` and `bootstrap.sh`. These scripts are explained in the following question.

### Q02: Analyze briefly the bootstrap.sh the host_ip.sh and the host_ssh.sh to interpret their purpose.

For all the nodes, we run the `hosts_ssh.sh` script. This script only allows key based logins. However, the `mgmt` box has some additional steps that need to be addressed. It starts by executing the `bootstrap.sh` script which installs a plethora of required software, including ansible (which will be used to manage the entire cluster later on). After the installation is finished, it will then execute the `hosts_ip.sh` script which will concatenate all of the virtual machines' ips (defined in the vagrant configuration) to its own `etc/hosts` file (this file contains ips up to 9 web servers, even though we're only using a total of 4). Finally, after all nodes run their specified scripts, they will restart.

### Q03: In what differs calling ansible --version from any directory, when compared by calling it from the Project directory you will launch?

After launching a terminal inside the `mgmt` node, by running `vagrant ssh mgmt`, if we execute the command `ansible --version` in the home directory, we are greeted with a descriptive message, stating the version as well as the locations to the configuration file, modules, executables, etc. The config file location shown in this case is `/etc/ansible/ansible.cfg`, which corresponds to the default config file of ansible.

However, if we change directory to the `tools` directory (the one that vagrant is sharing with the host), which contains the files we are using during the lab, and we run the same command again, we can see that the configuration file shown differs from the previous one. This time it states that it is using the one located within the same folder: `/home/vagrant/tools/ansible.cfg`. This means it detected we had a config file and that we want to use our own instead of the default one.

### Q04: After changing the Vagrantfile to accommodate it for launching more web servers, which were the modifications in the files of the Project that you have done? (just tell the name of the file and the lines that were changed)

To launch more web servers, we had to obviously launch more VMs (changed from 2 to 4 in the `Vagrantfile`) but we also had to make sure that the `mgmt` node knew of these new nodes. Given that the `hosts_ip.sh` already had ips for up to 9 servers, we didn't have to worry about that file. However, we did have to change the `inventory.ini` file to accommodate the new servers. To put simply, this file lets ansible know which servers are available, as well as their ips and how to connect (the user and the type of connection, ssh). It also lets us group them logically, i.e. **targets** (all nodes, except mgmt), **web** (all web nodes), etc.

### Q05: Write the result of the command ansible all -m shell -a "uptime" for your modified infrastructure.

![Output of `ansible all -m shell -a "uptime"`](pictures/89475_q5.png)

### Q06: Write the result of the command ansible all -m shell -a "uname -a" for your modified infrastructure.

![Output of `ansible all -m shell -a "uname -a"`](pictures/89475_q6.png)

### Q07: When deploying the Network Time Protocol (NTP) you have changed the Reference Time Servers, by modifying the Playbook in order to include a "variable" to be replaced in the NTP configuration file when Ansible runs that Playbook. However, there was a "bug" inadvertently written for this new procedure. Did you find the Bug? What corrections were made, for the NTP service to run?

The bug was related to the jinja variables from the playbook file. In the `ntp-template.yml` playbook file, there is a variable declartion for the `noc_ntpserver`. However, this variable also contained the prefix `server`. The config file that uses this variable, `ntp.conf.js`, also had the same prefix. this resulted in the duplication of the keyword, leading to `server server 0.europe.pool.ntp.org`. By deleting the `server` prefix in the `ntp-template.yml` file, the bug was fixed and everything worked accordingly (this file seems to be the most logical place to remove this keyword, as it should only contain the variable, and not the config server prefix).

### Q08: You ran the site\_(docker/vbox).yml Playbook. After running it for the second time, in case there were no errors that would prevent all tasks to complete, can you describe: What happened? What did you find different (or not)?

After the first execution, ansible's _Play Recap_ displayed all plays as **ok** (10 for the balancer, 8 for all the web nodes) and it showed that there had been changes, which means the tasks that were applied mutated the system (6 changes for the balancer, and 5 for the web nodes).

Upon running it again, the output was much simpler. Since ansible is idempotent, it will not execute changes unless there is something to change (the playbook file actually checks the state property to achieve this behaviour). This means that the _Play Recap_ this time showed 0 changes for all nodes. It instead limited itself to showing just **ok** (9 for the balancer, and 7 for the web nodes).

### Q09: When the system was deployed, when hitting the refresh button on the web browser (forcing with the Shift key): Did something change?

The html file that each web node is serving, makes use of a jinja variable populated by ansible, in particular, the hostname and the vm's static ip. When we refresh the page, the load balancer will choose the most appropriate server to serve our request. If we do the experiment of refreshing the web page a couple times, the hostname and the ip shown will change. For example, it can display `web3` and its ip one time, and after refreshing it will display `web4` and its corresponding ip. And if we keep refreshing it will show the remaining nodes as well.

### Q10: When using the Benchmarking tool, when using the concurrency parameter (-c) to a value still sustainable, What did you observe in the results of the Benchmark, were there errors, or failed requests? (just a brief interpretation).

Comparing the benchmarks between a concurrency of **2** and **20**, there was some change. The _Requests Per Second (RPS)_ metric was consistently 276. However the maximum time did increase roughly 500ms, and the average time also increased 10x (from 4.3ms to 46.5ms). This can be due to the fact that we only have 4 web servers available and we're sending 20 requests concurrently. It can also have to do with the load balancer being overloaded.

`sb.exe -n 1000 -c 2 -u http://localhost:8080/`
![Benchmark with concurrency 2](pictures/89475_q10_c2.png)

`sb.exe -n 1000 -c 20 -u http://localhost:8080/`
![Benchmark with concurrency 20](pictures/89475_q10_c20.png)
