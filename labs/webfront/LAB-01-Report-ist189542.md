# Lab-01 README

AGISIT 20201-2022

## Authors

**Team 08A**

![Tiago](../photos/ist189542-photo.png)


| Number | Name              | Username                                       | Email                                        |
| -------|-------------------|------------------------------------------------| ---------------------------------------------|
| 89542  | Tiago Fonseca     | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt>   |



## Q01: Interpret the Vagranfile that will be used, explaining, in your own words, what you think the “instructions” in it are supposed to do.

The main objective of this file is to build and manage the virtual machine environments in a single workflow, making it easier to setup and manage.

Going on more detail with the "instructions" in it, we can see three main regions:

- The first one, that is setting up the management node
- The second one that is setting up the load balancer
- The third one that is setting up the web servers

All regions have some common instructions. 
For instance, all regions set up the virtual machine provider, that in my case is virtual box, with the ammount of memory and cpus available. 
Other common "instruction" is the configuration of the network on each machine (the private network ip, ports being used, hostnames, etc ..). 
Lastly, all machines are told where to mount to, and are provisioned with some shell scripts, that will automatically install software or alter configurations.

---

## Q02: Analyze briefly the bootstrap.sh the host_ip.sh and the host_ssh.sh to interpret their purpose.

The bootstrap.sh file deals with software. It is responsible for installing and updating all needed packages for all the management node to work properly.

The host_ip.sh file is how the network is configured. As we can see, all servers have their own ip in order for the network to work properly. This provisioner allows the management node to be able to reach all the others, by saving all the ip on the known_hosts file.

Finally, the host_ssh.sh is the only provisioner that is provisioned to all the machines. It is essentially changing the authentication method of the ssh command, since the version of the OpenSSH on the virtualbox blocks logins with password. This provisioner allows this kind of logins. 

---

## Q03: In what differs calling ansible --version from any directory, when compared by calling it from the Project directory you will launch?

The difference is the config file value, that changes depending on which directory it is called. 

If we check the version on the project directory (assuming it is the "tools" directory), the config file will be point to the ansible.cfg file in that directory.

If perhaps we check the version on any other directory, the config file will be pointing to the "/etc/ansible/ansible.cfg" file, that is the default one. This config is overrided if there is an ansible.cfg file in the directory from where it is called (as in the Project directory).

---

## Q04: After changing the Vagrantfile to accommodate it for launching more web servers, which were the modifications in the files of the Project that you have done? (just tell the name of the file and the lines that were changed)

- Vagrantfile: Changed line #82 (1..2) => (1..5)
- inventory.ini: Uncommented line 9, 10, 20, 21 28, 29 and added a new line for web5 with a new ip, as well as an entrance on the web and targets values
- hosts_ip.sh: No changes were needed since it already had ips for 9 servers

---

## Q05: Write the result of the command ansible all -m shell -a "uptime" for your modified infrastructure.

The result is here

![uptime](pictures/89542_q5.png)

---

## Q06: Write the result of the command ansible all -m shell -a "uname -a" for your modified infrastructure.

The result is here

![uname -a](pictures/89542_q6.png)

---

## Q07: When deploying the Network Time Protocol (NTP) you have changed the Reference Time Servers, by modifying the Playbook in order to include a "variable" to be replaced in the NTP configuration file when Ansible runs that Playbook. However, there was a "bug" inadvertently written for this new procedure. Did you find the Bug? What corrections were made, for the NTP service to run?

Yes I found it. By analyzing the ntp-template.yml file, we can see at line 23 and 24 that ntp.conf is being written from ntp.conf.j2 to the /etc/ntp.conf file. So I looked into the file ntp.conf.j2 and saw that the line 7 has a "server {{ noc_ntpserver }}". This means that the variable being inputed should be the desired ntp_server. 
So, know that I knew how the configuration was being made, I looked into the ntp.conf of the web1 server to see what could be wrong with it.

![ntp.conf](pictures/89542_q7_1.png)

As we can see on line 8, there is something wrong since we have "server server". This made think that perhaps the bug was the way this "variable" was being inputed. So I looked again on the file ntp-template.yml and found the bug. It was on line 7 "noc_ntpserver: server 0.europe.pool.ntp.org". This is why the /etc/ntp.conf had the value "server" twice. 

To solve it, I removed the value server from this line 7, and I ended up with "noc_ntpserver: 0.europe.pool.ntp.org".

---

## Q08: You ran the site_(docker/vbox).yml Playbook. After running it for the second time, in case there were nor errors the would prevent all tasks to complete, can you describe: What happened? What did you find different (or not)?

This is the comparison of both runs:

First run                             |  Second run
:------------------------------------:|:--------------------------------------:
![first-run](pictures/89542_q8_1.png) | ![second-run](pictures/89542_q8_2.png)


On the first run, I can see that all servers changed, since we were installing nginx and placing all the desired configurations.
But on the second run, there is a different behaviour. Since Ansible is idempotent, this time there were no changes on any of the servers because the configuration and nginx was already in place. 

---

## Q09: When the system was deployed, when hitting the refresh button on the web browser (forcing with the Shift key): Did something change?

Yes it did. Everytime we hit the refresh button, the browser changed to a different server. This is because of the Load Balancer, that is distributing the traffic across all the available servers.

This is an example of what happened when I pressed the refresh button:

Before refresh                             |  After refresh
:-----------------------------------------:|:------------------------------------------:
![before-refresh](pictures/89542_q9_1.png) | ![after-refresh](pictures/89542_q9_2.png)

---

## Q10: When using the Benchmarking tool, when using the concurrency parameter (-c) to a value still sustainable, What did you observe in the results of the Benchmark, were there errors, or failed request? (just a brief interpretation).

With the benchmarking tool we were able to see how the system reacted to a lot of requests at the same time. 
In my case, since I had 5 servers, each one was receiving around 200 requests, meaning that the Load Balancer was working properly. By changing the -c value, I had no failed connections. I believe that the amount of concurrent requests was not large enough in order for some of them to fail.
The main difference was the Average Time that increased with a bigger "c" value, meaning that each server was taking more time to respond. This is a reasonable behaviour, since we were trying to send 20 concurrent requests while running only 5 servers.
