# LAB 01 Report - ist189429

AGISIT 20201-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png)


| Number | Name              | Username                                     | Email                               |
| -------|-------------------|----------------------------------------------| ------------------------------------|
| 89429  | Daniel Matos      | https://git.rnl.tecnico.ulisboa.pt/ist189429 | mailto:daniel.m.matos@tecnico.ulisboa.pt   |

## The Vagrant Environment

### Q01: Interpret the `Vagranfile` that will be used, explaining, in your own words, what you think the “instructions” in it are supposed to do.

The Vagrantfile creates a virtual machine called `mgmt` using `ubuntu/focal64`, a vagrant box, and creates a network where it is attributed the specified IP address. This VM also has 1 core and 2048 MB of RAM, uses the host to resolve DNS names and has a shared folder mounted in `/home/vagrant/tools`. A few scripts are used to modify the VM (`bootstrap.sh`, `hosts_ip.sh`, `hosts_ssh.sh`) and then it is restarted.

It also creates a virtual machine called `balancer` using the same resources as `mgmt`, only with a different IP of the same subnet and half of the RAM, has the same shared folder and it is only provisioned with the script `hosts_ssh.sh` before being restarted.

In the end, it creates 4 VMs called `webX` where `X` is replaced with a number from 1 to 4, with the same configuration as the `balancer` VM, except they have 512 MB of RAM and the IP is different, starting from `192.168.56.31` until `192.168.56.34` and the forwarded ports on the host range from `8081` until `8084`, depending on the number of the host (`X`).


### Q02: Analyze briefly the `bootstrap.sh`, the `hosts_ip.sh` and the `hosts_ssh.sh` to interpret their purpose.

`bootstrap.sh` installs Python 3 and other tools, adds the Ansible repository and installs it. In the end, cleans the downloaded package files to get more disk space.

`hosts_ip.sh` adds to `/etc/hosts` the "DNS Records" for the all ther virtual machines, so we can use human-friendly names (like `balancer`, `web1`, `web2`) instead of using the IPs. This works because `/etc/hosts` is checked before performing any DNS queries to any server.

`hosts_ssh.sh` changes `/etc/ssh/sshd_config` to allow password logins (in this box, it is disabled by default) by changing a the line where `PasswordAuthentication no` is located and change the `no` to `yes`. In the end, it reloads the SSH service daemon.


### Q03: In what differs calling `ansible --version` from any directory, when compared by calling it from the Project directory you will launch?

Calling `ansible --version` from the project directory makes this tool print that it will use the local configuration file (`ansible.cfg`), while outside this directory, it prints it will use the global one, located in `/etc/ansible/ansible.cfg`.


### Q04: After changing the `Vagrantfile` to accommodate it for launching more web servers, which were the modifications in the files of the Project that you have done? (just tell the name of the file and the lines that were changed)

To accomodate more servers, I only had to uncomment lines 9, 10, 20, 21, 28 and 29 in `inventory.ini`, besides changing from `2` to `4` on line 82 of the `Vagrantfile`.


### Q05: Write the result of the command `ansible all -m shell -a "uptime"` for your modified infrastructure.

```
balancer | CHANGED | rc=0 >>
 23:05:42 up 14 min,  1 user,  load average: 0.08, 0.04, 0.06
localhost | CHANGED | rc=0 >>
 23:05:41 up 15 min,  1 user,  load average: 0.00, 0.00, 0.00
web4 | CHANGED | rc=0 >>
 23:05:45 up 12 min,  1 user,  load average: 0.08, 0.06, 0.08
web3 | CHANGED | rc=0 >>
 23:05:47 up 12 min,  1 user,  load average: 0.15, 0.05, 0.04
web2 | CHANGED | rc=0 >>
 23:05:51 up 13 min,  1 user,  load average: 0.15, 0.05, 0.05
web1 | CHANGED | rc=0 >>
 23:05:51 up 14 min,  1 user,  load average: 0.15, 0.08, 0.10
```


### Q06: Write the result of the command `ansible all -m shell -a "uname -a"` for your modified infrastructure.

```
web2 | CHANGED | rc=0 >>
Linux web2 5.4.0-88-generic #99-Ubuntu SMP Thu Sep 23 17:29:00 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
web4 | CHANGED | rc=0 >>
Linux web4 5.4.0-88-generic #99-Ubuntu SMP Thu Sep 23 17:29:00 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
web3 | CHANGED | rc=0 >>
Linux web3 5.4.0-88-generic #99-Ubuntu SMP Thu Sep 23 17:29:00 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
web1 | CHANGED | rc=0 >>
Linux web1 5.4.0-88-generic #99-Ubuntu SMP Thu Sep 23 17:29:00 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
balancer | CHANGED | rc=0 >>
Linux balancer 5.4.0-88-generic #99-Ubuntu SMP Thu Sep 23 17:29:00 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
localhost | CHANGED | rc=0 >>
Linux mgmt 5.4.0-88-generic #99-Ubuntu SMP Thu Sep 23 17:29:00 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
```


### Q07: When deploying the Network Time Protocol (NTP) you have changed the Reference Time Servers, by modifying the Playbook in order to include a "variable" to be replaced in the NTP configuration file when Ansible runs that Playbook. However, there was a "bug" inadvertently written for this new procedure. Did you find the Bug? What corrections were made, for the NTP service to run?

A bug happened when deploying the `ntp-template.yml` playbook, because the files would contain a line starting with `server server`. In order to correct the bug, I changed the variable `noc_ntpserver` to contain only `0.europe.pool.ntp.org` instead of `server 0.europe.pool.ntp.org`. Another possible fix would be to edit `files/ntp.conf.j2` and change line 7 to `{{ noc_ntpserver }}` (only one should be applied, not both).


### Q08: You ran the `site_(docker/vbox).yml` Playbook. After running it for the second time, in case there were nor errors the would prevent all tasks to complete, can you describe: What happened? What did you find different (or not)?

On the second run of `site_vbox.yml` all nodes reported no changes, so they were all green, because everything in the played had already been done in the first run.


### Q09: When the system was deployed, when hitting the refresh button on the web browser (forcing with the Shift key): Did something change?

When the web page was refreshed, a different IP and hostname were shown, due to the load balancing provided by HA Proxy.


### Q10: When using the Benchmarking tool, when using the concurrency parameter (-c) to a value still sustainable, What did you observe in the results of the Benchmark, were tehre errors, or failed request? (just a brief interpretation).

When using the benchmarking tool, the requests took longer to be processed but there were no errors or failed requests. This happened because we changed from making 2 simultaneous requests (which could be easily handled by 4 replicas) to a bigger number of simultaneous requests, having each replica process more than 1 requests, which introduces latency in each requests, so it takes longer to process each one of them.
