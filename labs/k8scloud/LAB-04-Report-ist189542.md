# Lab-04 README

AGISIT 20201-2022

## Authors

**Team 08A**

![Tiago](../photos/ist189542-photo.png)


| Number | Name              | Username                                       | Email                                        |
| -------|-------------------|------------------------------------------------| ---------------------------------------------|
| 89542  | Tiago Fonseca     | <https://git.rnl.tecnico.ulisboa.pt/ist189542> | <mailto:tiagoatfonseca@tecnico.ulisboa.pt>   |


# Provisioning the Infrastructure with Terraform

## Q01: When you run the command terraform init which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed two plugins called. The first one was `hashicorp/google`, that is the Google provider used to configure Google Cloud Platform infrastructure and the second one was `hashicorp/kubernetes`, that allows for the management of all kubernetes resources.

![terraform init](pictures/89542_q1.png)

---

## Q02: Analyze briefly the terraform.tfvars and interpret its purpose.

The file contains variables that are used by other files, in order easily change the values when needed. There are 3 variables defined here. The first one is the `project id` that is a unique identifier for the project. The second variable is `workers_count` that defines how many nodes will be used for the cluster. And finally the `region` that is where we decide which region our application will be deployed in.

---

## Q03: Analyze briefly the gcp-gke-main.tf and interpret its purpose.

The `gcp-gke-main.tf` is where the Kubernetes Cluster is defined. The terraform.tfvars we described before is used in here, since all the variables are important for the creation of the cluster. In this file we also configure the OAuth2 access token that is used to authenticate API requests. At the end of the file, we also load the modules for the cluster and deploy the existing services and pods. More information about the `pods` and `services` are under the gcp_k8s dir.

---

## Q04: If you would need a larger Cluster (more Replicas) where and how would you declare that intention (within the Terraform files)?

We would have to change the `replica` value inside the spec block, on the file `k8s-pods.tf` under the gcp_k8s directory. This is not the final number of nodes that will be created, as we will see in the next question.

---

## Q05: When provisioning the infrastructure with Terraform, were there Errors related with Quota limits? If affirmative, what were the errors and did you solve them (or not...)?

Yes there were:

![error](pictures/89542_q5.png)


Since we have 3 replicas configured (previous question), with the value of 3 workers it would need 9 IPs (since we are dealing with 3 replicas and trying to use 3 workers, we would have 3 x 3 nodes created) in order to deploy every node in the selected region. Since the limmit is 8 IPs, to solve the error we must lower the number of workers, in order to not go over the limit.

---

## Q06: In case of success, which other files appeared in the k8scloud folder after running terraform apply for your infrastructure?.

Before running terraform apply, there fils present were the ones that were already on the repository and the terraform.tfvars we created on the first steps. 

After running the `terraform apply` two files appeared:

- The file `terraform.tfstate`. After some analysis on the file, I understood that it is a file were all the terraform state is stored. After some more investigation on the subject, I learned that it is where terraform stores all the state about our managed infrastructure and configuration. This is also used whenever we want to create a plan and make changes on our infrastructure. 
The main is than to store a binding between objects in a remote system and resource instances that are declared in our configuration.

- The file `terraform.tfstate.backup`. This file is pretty much the same as the previous one, except that as the name indicates, this one is a backup. It is used in the case that the original state file is lost or corrupted, allowing us to recover our configuration.

---

## Q07: In the gcp-gke-main.tf and in the k8s-provider.tf files, the module declarations or Provider declaration, require the specification of some variables, such as those related with RBAC (Role Base Access Control). Can you figure out and explain (briefly) how the "values" of those variables were obtained?

This values came from the `gcp_gke` module (`gcp_gke_cluster.tf`). At line 54 there is a region called "Output for the K8S". After the cluster is created, terraform is able to receive the `client_certificate`, `client_key`, `cluster_ca_certificate` and `host` from the kubernetes deployment. This variables are than passed to the gcp_k8s module as we can see an lines 38, 40, 41 and 42 of the `gcp-gke-main.tf`.

---

## Q08: In the k8s-pods.tf file, the declarations start with the Backend Pods, followed by the Frontend Pods. Do you think there is a reason for that order, or is it indifferent? HINT: Compare the sequence of operations using Terraform with the sequence suggested by Google for the manual deployment of the GuestBook, in their tutorial Create a guestbook with Redis and PHP

The Google manual deployment of the GuestBook created separated files for the backend and frontend deployment. In our file is indifferent if we define the backend pods after or before the frontend ones. Both pods will be deployed no matter the order. If we want to have a more structured file, we would define the backend first since the frontend is usually the one that depends on the backend. 

---

## Q09: When the system was fully deployed, which IP addresses of the Cluster nodes were reported?

Considering that in the next question we will be running kubectl (and with that we can get the IP addresses of the cluster nodes), another option to get access to those values is
by accessing the cluster information on the Google Cloud Platform. There we can go to the tab `Services & Ingress` and we can see all the running services and their respective endpoints. We can also have more information about each service if we click on its name.

---

## Q10: When you run kubectl get pods and kubectl get services what information was returned? Also, interpret what was returned with the command:
``$ gcloud container clusters describe guestbook --region <Region> --format='default(locations)'``

The information returned after running:

- 1 `kubectl get pods`
- 2 `kubectl get services`
- 3 `gcloud container clusters describe guestbook --region <Region> --format='default(locations)'`

![information returned](pictures/89542_q10.png)

`Kubectl` is a tool that lets use control Kubernetes clusters. The `kubectl get pods` command listed the information about all the pods (1 redis leader with 2 followers and 3 frontend replicas as defined on the `k8s-pods.tf`), and the `kubectl get services` command listed the information about all the services (it shows the load balancer and 1 service that is used to communicate with each replica, since the load balancer is the only one with a public IP). 

The `gcloud container clusters describe` command, as the name says, describes an existing cluster. In this case it returned the `europe-west1-b`, `europe-west1-c` and `europe-west1-d` region, since we did not selected a specific region on the terraform.tfvars file.
