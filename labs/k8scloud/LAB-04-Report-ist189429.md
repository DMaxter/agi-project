# LAB 04 Report - ist189429

AGISIT 20201-2022

## Authors

**Team 08A**

![Daniel](../photos/ist189429.png)


| Number | Name              | Username                                     | Email                               |
| -------|-------------------|----------------------------------------------| ------------------------------------|
| 89429  | Daniel Matos      | https://git.rnl.tecnico.ulisboa.pt/ist189429 | mailto:daniel.m.matos@tecnico.ulisboa.pt   |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command `terraform init` which plugins were installed? (You can copy the result of the command to insert in your report).

When I ran the command `terraform init`, it installed the plugin `google` so terraform could interact with Google Cloud Platform and manage the infrastructure there, and also installed the `kubernetes` plugin in order to manage the Kubernetes infrastructure, namely the cluster and the Kubernetes resources.

The output of the command is:

```
$ terraform init

Initializing modules...
- gcp_gke in gcp_gke
- gcp_k8s in gcp_k8s

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/google...
- Finding latest version of hashicorp/kubernetes...
- Installing hashicorp/google v4.0.0...
- Installed hashicorp/google v4.0.0 (signed by HashiCorp)
- Installing hashicorp/kubernetes v2.6.1...
- Installed hashicorp/kubernetes v2.6.1 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

### Q02: Analyze briefly the `terraform.tfvars` and interpret its purpose.

The file `terraform.tfvars` contains some variables that are used inside the other terraform files. It allows the user to change the value of some settings in only one place, instead of searching in every file and changing it there. It contains, in this case, the project ID, the number of worker nodes and the region in which the cluster will be deployed.

### Q03: Analyze briefly the `gcp-gke-main.tf` and interpret its purpose.

The file `gcp-gke-main.tf` prepares the configuration of the Google Kubernetes Engine cluster. It states the variables that it expects, where the client configuration is stored and also defines 2 modules (and their location) that will configure 2 different things:
- `gcp_gke` - which will have the cluster Kubernetes cluster configuration
- `gcp_k8s` - which will define the Pods and Services that will be run inside the cluster

### Q04: If you would need a larger Cluster (more Replicas) where and how would you declare that intention (within the Terraform files)?

If I would need a larger cluster, I would increase the variable `workers_count` in `terraform.tfvars` to a bigger value.

### Q05: When provisioning the infrastructure with Terraform, were there Errors related with Quota limits? If affirmative, what were the errors and did you solve them (or not...)?

Yes, there were errors, because I selected a region but not a zone, so my cluster got replicated in 3 zones inside that region, and since I defined 3 worker nodes, it tried to allocate 9 nodes in total (3 for each zone) which is above the limit of public IPs (limit is 8).
To solve this issue, I changed the number of worker nodes from 3 to 2, so the number decreased to 6 IPs.

### Q06: In case of success, which other files appeared in the `k8scloud` folder after running `terraform apply` for your infrastructure?.

After running `terraform apply`, a new file named `terraform.tfstate` where the state about the deployed infrastructure and configuration is stored.

### Q07: In the `gcp-gke-main.tf` and in the `k8s-provider.tf` files, the module declarations or Provider declaration, require the specification of some variables, such as those related with RBAC (Role Base Access Control). Can you figure out and explain (briefly) how the "values" of those variables were obtained?

The values that are used in `gcp-gke-main.tf` and `k8s-provider.tf` are needed so that the Kubernetes provider inside terraform is able to communicate with the Kubernetes master node at GKE. Those values are obtained automatically by terraform, by querying the Google infrastructure, as defined in `gcp_gke/gcp-gke-cluster.tf` in lines 56 through 74, as they are part of the output terraform should print after creating the cluster.

### Q08: In the `k8s-pods.tf` file, the declarations start with the Backend Pods, followed by the Frontend Pods. Do you think there is a reason for that order, or is it indifferent? HINT: Compare the sequence of operations using Terraform with the sequence suggested by Google for the manual deployment of the GuestBook, in their tutorial [Create a guestbook with Redis and PHP](https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook)

The reason for declaring first the backend pods and only after the frontend pods, is because the frontend pods will depend on the backend ones. However, the order doesn't matter here, because we didn't specify any dependency using Terraform and Kubernetes assumes that the pods don't have dependencies between them. So, everything will be deployed at the same time, contrary to what is suggested by Google.

### Q09: When the system was fully deployed, which IP addresses of the Cluster nodes were reported?

The IP addresses of the cluster nodes can be obtained by running the following command (with the IPs in the output):

```
$ kubectl get nodes -o wide
NAME                                       STATUS   ROLES    AGE   VERSION             INTERNAL-IP   EXTERNAL-IP      OS-IMAGE                             KERNEL-VERSION   CONTAINER-RUNTIME
gke-guestbook-default-pool-57ea920f-32xg   Ready    <none>   25m   v1.20.10-gke.1600   10.132.0.13   35.240.124.244   Container-Optimized OS from Google   5.4.120+         containerd://1.4.4
gke-guestbook-default-pool-57ea920f-hnh9   Ready    <none>   25m   v1.20.10-gke.1600   10.132.0.12   35.190.197.158   Container-Optimized OS from Google   5.4.120+         containerd://1.4.4
gke-guestbook-default-pool-804dcc99-76xr   Ready    <none>   25m   v1.20.10-gke.1600   10.132.0.10   34.76.251.111    Container-Optimized OS from Google   5.4.120+         containerd://1.4.4
gke-guestbook-default-pool-804dcc99-l35l   Ready    <none>   25m   v1.20.10-gke.1600   10.132.0.11   104.199.4.176    Container-Optimized OS from Google   5.4.120+         containerd://1.4.4
gke-guestbook-default-pool-9a77dfde-6wks   Ready    <none>   25m   v1.20.10-gke.1600   10.132.0.9    35.240.123.4     Container-Optimized OS from Google   5.4.120+         containerd://1.4.4
gke-guestbook-default-pool-9a77dfde-cnbh   Ready    <none>   25m   v1.20.10-gke.1600   10.132.0.8    34.79.183.93     Container-Optimized OS from Google   5.4.120+         containerd://1.4.4
```


### Q10: When you run `kubectl get pods` and `kubectl get services` what information was returned? Also, interpret what was returned with the command:

```
$ gcloud container clusters describe guestbook --region <Region> --format='default(locations)'
```

When running `kubectl get pods`, I can observe the status of each pod (if it is running or not), if it has been restarted and for how long it is running. Here is the output:

```
$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
frontend-555584b8c9-f2nsr         1/1     Running   0          26m
frontend-555584b8c9-h5ndj         1/1     Running   0          26m
frontend-555584b8c9-xtqk6         1/1     Running   0          26m
redis-follower-6579bcb987-c25v2   1/1     Running   0          26m
redis-follower-6579bcb987-kpjhv   1/1     Running   0          26m
redis-leader-769c885c4f-tkqg4     1/1     Running   0          26m
```

With `kubectl get services`, I can see all services that are present in my Kubernetes cluster, along with their type, internal IP, external IP, the port mapping and also how old the service is. The output of the command was:

```
$ kubectl get services
NAME             TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
frontend         LoadBalancer   10.3.242.163   34.77.50.33   80:30438/TCP   28m
kubernetes       ClusterIP      10.3.240.1     <none>        443/TCP        29m
redis-follower   ClusterIP      10.3.248.60    <none>        6379/TCP       28m
redis-leader     ClusterIP      10.3.253.56    <none>        6379/TCP       28m
```

The command `gcloud container clusters describe guestbook --region <Region> --format='default(locations)'` only shows the location of the nodes of our cluster, i.e., the zones where the nodes are located. The output of the command was the following:

```
$ gcloud container clusters describe guestbook --region europe-west1 --format='default(lo
cations)'
locations:
- europe-west1-b
- europe-west1-d
- europe-west1-c
```
