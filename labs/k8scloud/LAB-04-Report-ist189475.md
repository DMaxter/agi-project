# Lab 04 Report - ist189475

AGISIT 2021-2022

## Authors

**Team 08A - João Soares**

![João Soares](../photos/jsoares.jpg)

| Number | Name        | Username                                       | Email                                     |
| ------ | ----------- | ---------------------------------------------- | ----------------------------------------- |
| 89475  | João Soares | <https://git.rnl.tecnico.ulisboa.pt/ist189475> | <mailto:joao.m.soares@tecnico.ulisboa.pt> |

## Provisioning the Infrastructure with Terraform

### Q01: When you run the command `terraform init` which plugins were installed? (You can copy the result of the command to insert in your report).

Terraform installed 2 plugins: hashicorp's `google` and `kubernetes` modules. The output of the command can be seen below:

![Output of `terraform init`](pictures/89475_q1.png)

### Q02: Analyze briefly the `terraform.tfvars` and interpret its purpose.

This file contains the variables used by terraform to provision the infrastructure. In this case, it contains 3 variables:

- _project_: the project name in the Google Cloud Platform (GCP)
- _workers_count_: the number of worker nodes in the cluster
- _region_: the region where the cluster should be deployed

### Q03: Analyze briefly the `gcp-gke-main.tf` and interpret its purpose.

This file contains the main configuration for the whole cluster. It starts by declaring the variables mentioned previously. It also declares a data structure that will hold oauth information for the kubernetes module to use. Finally, it loads the modules for the cluster infrastructure (`gcp_gke`) as well as the deployments and services (`gcp_k8s`). These modules act like nested instances of terraform, so we can pass the variables in the body of the definition and use them normally within the configuration files of each module.

### Q04: If you would need a larger Cluster (more Replicas) where and how would you declare that intention (within the Terraform files)?

The `gcp_k8s/k8s-pods.tf` file contains the number of replicas for the `frontend` deployment. In this case the number of replicas is **3**. This is defined inside the `spec` block.
This value is not the same as the number of worker nodes. In fact, the total number of nodes in the cluster is the multiplication of these two numbers. In our case, we will have **2** worker nodes in each zone. The number of zones is the same as the number of replicas. This gives us a total of **6** nodes (2 workers replicated 3 times).

### Q05: When provisioning the infrastructure with Terraform, were there Errors related with **Quota limits**? If affirmative, what were the errors and did you solve them (or not...)?

Initially, the deployment prepared was of 3 worker nodes in region `europe-west3`. Unfortunately, this did not work as google limits the number of assigned IP addresses to 8. Since we have 3 replicas configured, this would result in **9** nodes, each requiring its own IP address. To fix this issue, we simply have to change the number of worker nodes to **2**. This will lead to a total of 6 nodes in the cluster.

### Q06: In case of success, which other files appeared in the `k8scloud` folder after running `terraform apply` for your infrastructure?

Upon running the specified command, a new file was created: `terraform.tfstate`. This file contains all the information about the current state of the deployed infrastructure as well as all the defined outputs in the `terraform-gcp-outputs.tf` configuration file. In case it is not the first time that the command is run, a backup file `terraform.tfstate.backup` is created as well.

### Q07: In the `gcp-gke-main.tf` and in the `k8s-provider.tf` files, the module declarations or Provider declaration, require the specification of some _variables_, such as those related with RBAC (Role Base Access Control). Can you figure out and explain (briefly) how the "values" of those _variables_ were obtained?

The variables passed to the `gcp_k8s` module come directly from the deployment of the other module. After creating the cluster, terraform receives the kubernetes credentials (host, certificate, key and ca).

The `gcp_gke/gcp_gke_cluster.tf` file contains the outputs that can be accessed directly through the module object. The values can be accessed by the names of the outputs.

### Q08: In the `k8s-pods.tf` file, the declarations start with the Backend Pods, followed by the Frontend Pods. Do you think there is a reason for that order, or is it indifferent? **HINT**: Compare the sequence of operations using Terraform with the sequence suggested by Google for the manual deployment of the GuestBook, in their tutorial [Create a guestbook with Redis and PHP](https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook)

Keepind the backend pods above the frontend pods makes sense. The frontend depends on the backend logic to perform its duty. However, in this case, moving the configuration of the frontend pod to the top of the file does not change the deployment process in any way.

### Q09: When the system was fully deployed, which IP addresses of the Cluster nodes were reported?

By running `kubectl get services` we can see a list of the services deployed. These are assigned Cluster IP addresses, which are not publicly accessible. However, the Load Balancer has an external IP address, which can be used to publicly access that service and its deployments.

### Q10: When you run `kubectl get pods` and `kubectl get services` what information was returned? Also, interpret what was returned with the command:

```bash
$ gcloud container clusters describe guestbook --region <Region> --format='default(locations)'
```

Retrieving the pods shows that there are 2 `redis-follower` deployments (because it is specified that there should be **2** replicas of this pod), 1 `redis-leader` (no replicas) and 3 `frontend` deployements (**3** replicas specified).

![Output of `kubectl get pods`](pictures/89475_q10_a.png)

Retrieving the services shows that there is a total of 4 services: **1** LoadBalancer and **3** ClusterIP. The LoadBalancer is the only service with an external IP address and consequently the only publicly accessible endpoint. The ClusterIP services are for communicating within the internal kubernetes network.

![Output of `kubectl get services`](pictures/89475_q10_b.png)

Finally, running the specified `gcloud` command shows that the cluster is deployed accross **3** zones in the specified region (`europe-west1`) to accommodate the replicas, since the higher value of replicas we have is **3** (frontend).

![Output of `kubectl get services`](pictures/89475_q10_c.png)
